
DecalFiltersps.dll: dlldata.obj DecalFilters_p.obj DecalFilters_i.obj
	link /dll /out:DecalFiltersps.dll /def:DecalFiltersps.def /entry:DllMain dlldata.obj DecalFilters_p.obj DecalFilters_i.obj \
		kernel32.lib rpcndr.lib rpcns4.lib rpcrt4.lib oleaut32.lib uuid.lib \

.c.obj:
	cl /c /Ox /DWIN32 /D_WIN32_WINNT=0x0400 /DREGISTER_PROXY_DLL \
		$<

clean:
	@del DecalFiltersps.dll
	@del DecalFiltersps.lib
	@del DecalFiltersps.exp
	@del dlldata.obj
	@del DecalFilters_p.obj
	@del DecalFilters_i.obj
