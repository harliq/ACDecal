
#include "stdafx.h"
#include "DecalFilters.h"
#include "AllegianceInfo.h"

/////////////////////////////////////////////////////////////////////////////

STDMETHODIMP cAllegianceInfo::get_Name(BSTR *pVal)
{
	*pVal = T2BSTR( m_pAllegiance->Name );

	return S_OK;
}

STDMETHODIMP cAllegianceInfo::get_GUID(long *pVal)
{
	*pVal = m_pAllegiance->GUID;

	return S_OK;
}

STDMETHODIMP cAllegianceInfo::get_TreeParent(long *pVal)
{
	*pVal = m_pAllegiance->TreeParent;

	return S_OK;
}

STDMETHODIMP cAllegianceInfo::get_Type(long *pVal)
{
	*pVal = m_pAllegiance->Type;

	return S_OK;
}

STDMETHODIMP cAllegianceInfo::get_XP(long *pVal)
{
	*pVal = m_pAllegiance->XP;

	return S_OK;
}

STDMETHODIMP cAllegianceInfo::get_Loyalty(long *pVal)
{
	*pVal = m_pAllegiance->Loyalty;

	return S_OK;
}

STDMETHODIMP cAllegianceInfo::get_Leadership(long *pVal)
{
	*pVal = m_pAllegiance->Leadership;

	return S_OK;
}

STDMETHODIMP cAllegianceInfo::get_Gender(long *pVal)
{
	*pVal = m_pAllegiance->Gender;

	return S_OK;
}

STDMETHODIMP cAllegianceInfo::get_Race(long *pVal)
{
	*pVal = m_pAllegiance->Race;

	return S_OK;
}

STDMETHODIMP cAllegianceInfo::get_Rank(long *pVal)
{
	*pVal = m_pAllegiance->Rank;

	return S_OK;
}

STDMETHODIMP cAllegianceInfo::get_Unknown(double *pVal)
{
	*pVal = m_pAllegiance->Unknown;

	return S_OK;
}
