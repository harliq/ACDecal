//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by DecalFilters.rc
//
#define IDS_PROJNAME                    100
#define IDR_INVENTORY                   101
#define IDR_INVENTORYITEM               102
#define IDR_WORLD                       103
#define IDR_WORLDOBJECT                 104
#define IDR_STACKABLE                   107
#define IDR_LOCATION                    108
#define IDR_WORLDITERATOR               109
#define IDR_IDENTIFYQUEUE               110
#define IDR_ECHOFILTER                  202
#define IDR_CHARACTERSTATS              204
#define IDR_PREFILTER                   205
#define IDR_ECHOFILTER2                 207

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        208
#define _APS_NEXT_COMMAND_VALUE         32768
#define _APS_NEXT_CONTROL_VALUE         201
#define _APS_NEXT_SYMED_VALUE           111
#endif
#endif
