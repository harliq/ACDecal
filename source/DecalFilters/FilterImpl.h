// FilterImpl.h
// Declaration of template classes to help network filters

#ifndef __FILTERIMPL_H
#define __FILTERIMPL_H

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

template< UINT nID, class cImpl, const IID *pIID > 
class IFilterEventsImpl
: public IDispEventImpl< nID, cImpl, pIID, &LIBID_DecalFilters, 1, 0 >
{
public:
   HRESULT advise( IUnknown *pUnk )
   {
      return DispEventAdvise( pUnk );
   }
   
   HRESULT unadvise( IUnknown *pUnk )
   {
      return DispEventUnadvise( pUnk );
   }
};


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

template< UINT nID, class cImpl >
class IWorldEventsImpl
: public IFilterEventsImpl< nID, cImpl, &DIID_IWorldEvents >
{
};

#define DISPID_CREATE_OBJECT  1
#define DISPID_RELEASE_OBJECT 2
#define DISPID_CHANGE_OBJECT  3
#define DISPID_MOVED_OBJECT   4


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

template< UINT nID, class cImpl >
class IEchoEventsImpl
: public IFilterEventsImpl< nID, cImpl, &DIID_IEchoSink >
{
};

#define DISPID_ECHO_MESSAGE 1


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

template< UINT nID, class cImpl >
class IEcho2EventsImpl
: public IFilterEventsImpl< nID, cImpl, &DIID_IEchoSink2 >
{
};

#define DISPID_ECHO_SERVER 1
#define DISPID_ECHO_CLIENT 2


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

template< UINT nID, class cImpl >
class ICharacterStatsEventsImpl
: public IFilterEventsImpl< nID, cImpl, &DIID_ICharacterStatsEvents >
{
};

#define DISPID_LOGIN            1
#define DISPID_SPELLBOOK_ADD    2
#define DISPID_SPELLBOOK_DELETE 3

#endif
