// Enchantment.cpp : Implementation of CDecalFiltersApp and DLL registration.

#include "stdafx.h"
#include "DecalFilters.h"
#include "Enchantment.h"

/////////////////////////////////////////////////////////////////////////////
//

STDMETHODIMP Enchantment::get_SpellID(long *pVal)
{
	*pVal = SpellID;

	return S_OK;
}

STDMETHODIMP Enchantment::put_SpellID(long newVal)
{
	SpellID = newVal;

	return S_OK;
}

STDMETHODIMP Enchantment::get_Layer(long *pVal)
{
	*pVal = Layer;

	return S_OK;
}

STDMETHODIMP Enchantment::put_Layer(long newVal)
{
	Layer = newVal;

	return S_OK;
}

STDMETHODIMP Enchantment::get_TimeRemaining(long *pVal)
{
	*pVal = SecondsLeft;

	return S_OK;
}

STDMETHODIMP Enchantment::put_TimeRemaining(long newVal)
{
	SecondsLeft = newVal;

	return S_OK;
}

STDMETHODIMP Enchantment::get_Affected(long *pVal)
{
	*pVal = Affected;

	return S_OK;
}

STDMETHODIMP Enchantment::put_Affected(long newVal)
{
	Affected = newVal;

	return S_OK;
}

STDMETHODIMP Enchantment::get_AffectedMask(long *pVal)
{
	*pVal = AffectMask;

	return S_OK;
}

STDMETHODIMP Enchantment::put_AffectedMask(long newVal)
{
	AffectMask = newVal;

	return S_OK;
}

STDMETHODIMP Enchantment::get_Family(long *pVal)
{
	*pVal = Family;

	return S_OK;
}

STDMETHODIMP Enchantment::put_Family(long newVal)
{
	Family = newVal;

	return S_OK;
}

STDMETHODIMP Enchantment::get_Adjustment(double *pVal)
{
	*pVal = Adjustment;

	return S_OK;
}

STDMETHODIMP Enchantment::put_Adjustment(double newVal)
{
	Adjustment = newVal;

	return S_OK;
}
