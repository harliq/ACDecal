// AttributeInfo.cpp : Implementation of cAttributeInfo
#include "stdafx.h"
#include "DecalFilters.h"
#include "AttributeInfo.h"

/////////////////////////////////////////////////////////////////////////////
// cAttributeInfo

STDMETHODIMP cAttributeInfo::get_Name(BSTR *pVal)
{
   if( pVal == NULL )
   {
      _ASSERT( FALSE );
      return E_POINTER;
   }

   USES_CONVERSION;

   *pVal = T2BSTR( m_pInfo->m_szName.c_str() );

	return S_OK;
}

STDMETHODIMP cAttributeInfo::get_Creation(long *pVal)
{
   if( pVal == NULL )
   {
      _ASSERT( FALSE );
      return E_POINTER;
   }

   *pVal = m_pInfo->m_nBase;

	return S_OK;
}

STDMETHODIMP cAttributeInfo::get_Exp(long *pVal)
{
   if( pVal == NULL )
   {
      _ASSERT( FALSE );
      return E_POINTER;
   }

   *pVal = m_pInfo->m_nExp;

	return S_OK;
}

STDMETHODIMP cAttributeInfo::get_Current(long *pVal)
{
   if( pVal == NULL )
   {
      _ASSERT( FALSE );
      return E_POINTER;
   }

   *pVal = m_pInfo->m_nCurrent;

	return S_OK;
}
