// SolidImage.cpp : Implementation of cSolidImage
#include "stdafx.h"
#include "Inject.h"
#include "SolidImage.h"

/////////////////////////////////////////////////////////////////////////////
// cSolidImage


STDMETHODIMP cBrushImage::get_Color(long *pVal)
{
   _ASSERTE( pVal != NULL );

   *pVal = m_nColor;

	return S_OK;
}

STDMETHODIMP cBrushImage::put_Color(long newVal)
{
   // Make sure the color is in range
   _ASSERTE( ( newVal & 0xFF000000 ) == 0 );

   m_nColor = newVal;

	return S_OK;
}
