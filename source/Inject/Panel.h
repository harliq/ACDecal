// Panel.h : Declaration of the cPanel

#ifndef __PANEL_H_
#define __PANEL_H_

#include "resource.h"       // main symbols

#include "SinkImpl.h"
#include "View.h"

#define BUTTON_CLOSE 1
#define BUTTON_INC 2
#define BUTTON_DEC 3

/////////////////////////////////////////////////////////////////////////////
// cPanel
class ATL_NO_VTABLE cPanel : 
	public CComObjectRootEx<CComMultiThreadModel>,
	public ILayerRenderImpl,
	public ILayerImpl< cPanel >,
	public ILayerMouseImpl,
	public ICommandEventsImpl< BUTTON_CLOSE, cPanel >,
	public ICommandEventsImpl< BUTTON_INC, cPanel >,
	public ICommandEventsImpl< BUTTON_DEC, cPanel >,
	public cNoEventsImpl,
	public IPanel
{
public:
	cPanel();

   long m_nActiveView;

   CComPtr< IImageCache > m_pBorder;
   CComPtr< IImageCache > m_pBackground;
   CComPtr< IFontCache > m_pTitle;
   CComPtr< IPanelSink > m_pSink;
   CComPtr< IButton > m_pButtonInc;
   CComPtr< IButton > m_pButtonDec;

   ViewParams* m_pVP;
   cView* m_pcView;

   DWORD m_Alpha;

   long m_DeltaX;
   long m_DeltaY;

   bool m_bDragging;
   bool m_bSizingXL;
   bool m_bSizingXR;
   bool m_bSizingYT;
   bool m_bSizingYB;
   bool m_bTransparent;

   void hideView();

   void onCreate();
   void onDestroy();

BEGIN_COM_MAP(cPanel)
	COM_INTERFACE_ENTRY(ILayer)
	COM_INTERFACE_ENTRY(ILayerRender)
	COM_INTERFACE_ENTRY(IPanel)
	COM_INTERFACE_ENTRY(ILayerMouse)
END_COM_MAP()

BEGIN_SINK_MAP( cPanel )
   SINK_ENTRY_EX( BUTTON_CLOSE, DIID_ICommandEvents, DISPID_ACCEPTED, onCloseAccepted )
   SINK_ENTRY_EX( BUTTON_INC, DIID_ICommandEvents, DISPID_ACCEPTED, onAlphaInc )
   SINK_ENTRY_EX( BUTTON_DEC, DIID_ICommandEvents, DISPID_ACCEPTED, onAlphaDec )
END_SINK_MAP()

// IPanel
public:
	STDMETHOD(LoadView)(long nPlugin, IView *pView, IUnknown *pSchema);
	STDMETHOD(LoadViewEx)(long nPlugin, IView *pView, IUnknown *pSchema, long lViewFlags);
	STDMETHOD(get_ActiveView)(/*[out, retval]*/ long *pVal);
	STDMETHOD(AddView)(long nPluginID, ILayer *pLayer);
	STDMETHOD(RemoveView)(long nID);
	STDMETHOD(ActivateView)(long nViewID, ViewParams *pParams, long *pVal);
	STDMETHOD(Deactivate)();
	STDMETHOD(putref_Sink)(/*[in]*/ IPanelSink* newVal);
	STDMETHOD(get_Transparent)(/*[out, retval]*/ VARIANT_BOOL *pVal);
	STDMETHOD(put_Transparent)(/*[in]*/ VARIANT_BOOL newVal);
	STDMETHOD(put_Params)(ViewParams *Params);

   // ILayerRender Methods
   STDMETHOD(Render)(ICanvas *);
   STDMETHOD(Reformat)();

   // ICommandEvents Methods
   void __stdcall onCloseAccepted(long nID);
   void __stdcall onAlphaInc(long nID);
   void __stdcall onAlphaDec(long nID);

   // ILayerMouse Methods
   STDMETHOD(MouseEvent)(long nMsg, long wParam, long lParam);
};

#endif //__PANEL_H_
