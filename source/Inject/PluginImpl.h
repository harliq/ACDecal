// PluginImpl.h
// Declaration of helper class for implementing plugins

#ifndef __PLUGINIMPL_H
#define __PLUGINIMPL_H

template< class cImpl >
class IPluginImpl
: public IPlugin
{
public:
   CComPtr< IPluginSite > m_pSite;

   void onInitialize()
   {
   }

   void onTerminate()
   {
   }

   static LPCTSTR name()
   {
      // You must implement this function in the derived class
      _ASSERTE( FALSE );

      return _T( "ERROR: <unnamed plugin>" );
   }

   STDMETHOD(Initialize)( IPluginSite *pSite, long nID )
   {
      m_pSite = pSite;

      static_cast< cImpl * >( this )->onInitialize();

      return S_OK;
   }

   STDMETHOD(Terminate)()
   {
      static_cast< cImpl * >( this )->onTerminate();

      m_pSite.Release();

      return S_OK;
   }

   STDMETHOD(get_FriendlyName)( BSTR *pbstrName )
   {
      _ASSERTE( pbstrName != NULL );

      *pbstrName = T2BSTR( cImpl::name() );
      return S_OK;
   }
};

// Included here for backward compatibility
#include <EventsImpl.h>

#endif // __PLUGINIMPL_H
