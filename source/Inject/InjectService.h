// InjectService.h : Declaration of the cInjectService

#ifndef __INJECTSERVICE_H_
#define __INJECTSERVICE_H_

#include "resource.h"       // main symbols
#include <DecalImpl.h>

/////////////////////////////////////////////////////////////////////////////
// cInjectService
class ATL_NO_VTABLE cInjectService : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<cInjectService, &CLSID_InjectService>,
	public IInjectService,
   public IDecalServiceImpl<cInjectService>,
   public IDecalDirectory
{
public:
	cInjectService()
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_INJECTSERVICE)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cInjectService)
	COM_INTERFACE_ENTRY(IInjectService)
   COM_INTERFACE_ENTRY(IDecalService)
   COM_INTERFACE_ENTRY(IDecalDirectory)
END_COM_MAP()

// IDecalService
   STDMETHOD(BeforePlugins)();
   STDMETHOD(AfterPlugins)();

// IDecalDirectory
   STDMETHOD(Lookup)(BSTR strName, IUnknown **ppvItf)
   {
      if ( ::wcsicmp ( strName, L"site" ) != 0 )
         return E_INVALIDARG;

      return get_Site ( reinterpret_cast< IPluginSite ** > ( ppvItf ) );
   }

// IInjectService
public:
	STDMETHOD(InitPlugin)(IUnknown *pUnk);
	STDMETHOD(get_Site)(/*[out, retval]*/ IPluginSite **pVal);
};

#endif //__INJECTSERVICE_H_
