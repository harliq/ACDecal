// View.h : Declaration of the cView

#ifndef __VIEW_H_
#define __VIEW_H_

#include "resource.h"       // main symbols
#include "InjectCP.h"

class cPanel;
class cRootLayer;

/////////////////////////////////////////////////////////////////////////////
// cView
class ATL_NO_VTABLE cView : 
	public CComObjectRootEx<CComMultiThreadModel>,
	public IConnectionPointContainerImpl<cView>,
    public IProvideClassInfo2Impl< &CLSID_View, &DIID_IViewEvents, &LIBID_DecalPlugins >,
	public IDispatchImpl< IView, &IID_IViewDisp, &LIBID_DecalPlugins >,
	public CProxyIViewEvents< cView >
{
public:
	cView( )
	{
		m_VP.alpha = -1;
		m_lOldAlpha = 255;
	}

   ~cView();

   cRootLayer *m_pRoot;

   long m_nViewID;

   bool		m_bActivated;

   CComPtr< IPanel > m_pPanel;
      
   ViewParams m_VP;
   long		m_lOldAlpha;
   bool		m_bTransparent;

   // Named controls
   struct cNamedControl
   {
      _bstr_t m_strName;
      CComPtr< IControl > m_pControl;
   };

   typedef std::list< cNamedControl > cNamedControlList;
   cNamedControlList m_controls;

   long loadSchema( BSTR strText, IUnknown **ppRootControl );
   long loadSchemaObject( IUnknown *pObject, IUnknown **ppRootControl );

BEGIN_COM_MAP(cView)
	COM_INTERFACE_ENTRY(IView)
	COM_INTERFACE_ENTRY(IViewDisp)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IProvideClassInfo)
	COM_INTERFACE_ENTRY(IProvideClassInfo2)
	COM_INTERFACE_ENTRY(IConnectionPointContainer)
	COM_INTERFACE_ENTRY_IMPL(IConnectionPointContainer)
END_COM_MAP()

BEGIN_CONNECTION_POINT_MAP(cView)
CONNECTION_POINT_ENTRY(DIID_IViewEvents)
END_CONNECTION_POINT_MAP()

public:
   // IView Methods
	STDMETHOD(SetIcon)(long icon, /*[optional]*/ VARIANT iconlibrary);
	STDMETHOD(Activate)();
	STDMETHOD(Alert)();
	STDMETHOD(get_Title)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_Title)(/*[in]*/ BSTR newVal);
	STDMETHOD(LoadSchema)(BSTR strXMLSchema);
	STDMETHOD(LoadControl)(ILayerSite *pParent, long nID, IUnknown *pSource, long *pAssignedID);
	STDMETHOD(get_Control)(BSTR strName, /*[out, retval]*/ IControl * *pVal);
	STDMETHOD(putref_Control)(BSTR strName, /*[in]*/ IControl * newVal);
	STDMETHOD(Deactivate)();
	STDMETHOD(put_Position)(/*[in]*/ RECT *newVal);
	STDMETHOD(get_Position)(/*[out, retval]*/ RECT *pVal);	
	STDMETHOD(put_Alpha)(long Alpha);
	STDMETHOD(get_Alpha)(long *pVal);
	STDMETHOD(get_Activated)(/*[out, retval]*/ VARIANT_BOOL *pVal);
	STDMETHOD(put_Activated)(/*[in]*/ VARIANT_BOOL newVal);
	STDMETHOD(get_Transparent)(/*[out, retval]*/ VARIANT_BOOL *pVal);
	STDMETHOD(put_Transparent)(/*[in]*/ VARIANT_BOOL newVal);
};

#endif //__VIEW_H_

