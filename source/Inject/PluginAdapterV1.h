// PluginAdapterV1.h : Declaration of the cPluginAdapterV1

#ifndef __PLUGINADAPTERV1_H_
#define __PLUGINADAPTERV1_H_

#include "resource.h"       // main symbols
#include <Decal.h>

/////////////////////////////////////////////////////////////////////////////
// cPluginAdapterV1
class ATL_NO_VTABLE cPluginAdapterV1 : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<cPluginAdapterV1, &CLSID_PluginAdapterV1>,
	public IPluginAdapterV1,
   public IPlugin2,
   public IDecalSurrogate
{
public:
	cPluginAdapterV1()
	{
	}

   CComPtr<IPluginSite2> m_pSite2;
   CComPtr<IUnknown> m_pUnkPlugin;

DECLARE_REGISTRY_RESOURCEID(IDR_PLUGINADAPTERV1)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cPluginAdapterV1)
   COM_INTERFACE_ENTRY(IDecalSurrogate)
   COM_INTERFACE_ENTRY(IPlugin2)
	COM_INTERFACE_ENTRY(IPluginAdapterV1)
   COM_INTERFACE_ENTRY_AGGREGATE_BLIND(m_pUnkPlugin.p)
END_COM_MAP()

// IPluginAdapterV1
public:

   // IPlugin2
   STDMETHOD(Initialize)(IPluginSite2 *pSite2);
   STDMETHOD(Terminate)();

   // IDecalSurrogate
   STDMETHOD(CreateInstance)(IDecalEnum *pEnum, REFIID riid, LPVOID *ppvItf);
};

#endif //__PLUGINADAPTERV1_H_
