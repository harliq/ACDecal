#ifndef __DIRECTDRAWSURFACEHOOK_H_
#define __DIRECTDRAWSURFACEHOOK_H_

#include "resource.h"

#define BEGIN_COM_MAP_EX(x) \
public: typedef x _ComMapClass; \
static HRESULT WINAPI _Cache(void* pv, REFIID iid, void** ppvObject, DWORD dw) \
{	\
	_ComMapClass* p = (_ComMapClass*)pv;	\
	HRESULT hRes = CComObjectRootBase::_Cache(pv, iid, ppvObject, dw);	\
	return hRes;	\
}	\
IUnknown* _GetRawUnknown()	\
{ ATLASSERT(_GetEntries()[0].pFunc == _ATL_SIMPLEMAPENTRY); return (IUnknown*)((int)this+_GetEntries()->dw); }	\
_ATL_DECLARE_GET_UNKNOWN(x)	\
HRESULT _InternalQueryInterface(REFIID iid, void** ppvObject)	\
{ return InternalQueryInterface(this, _GetEntries(), iid, ppvObject); } \
const static _ATL_INTMAP_ENTRY* WINAPI _GetEntries() { \
static const _ATL_INTMAP_ENTRY _entries[] = { DEBUG_QI_ENTRY(x)

/*

#define BEGIN_COM_MAP(x) public: \
	typedef x _ComMapClass; \
	static HRESULT WINAPI _Cache(void* pv, REFIID iid, void** ppvObject, DWORD_PTR dw)\
	{\
		_ComMapClass* p = (_ComMapClass*)pv;\
		p->Lock();\
		HRESULT hRes = CComObjectRootBase::_Cache(pv, iid, ppvObject, dw);\
		p->Unlock();\
		return hRes;\
	}\
	IUnknown* _GetRawUnknown() \
	{ ATLASSERT(_GetEntries()[0].pFunc == _ATL_SIMPLEMAPENTRY); return (IUnknown*)((DWORD_PTR)this+_GetEntries()->dw); } \
	_ATL_DECLARE_GET_UNKNOWN(x)\
	HRESULT _InternalQueryInterface(REFIID iid, void** ppvObject) \
	{ return InternalQueryInterface(this, _GetEntries(), iid, ppvObject); } \
	const static _ATL_INTMAP_ENTRY* WINAPI _GetEntries() { \
	static const _ATL_INTMAP_ENTRY _entries[] = { DEBUG_QI_ENTRY(x)


/////
public: typedef CDirectDrawSurfaceHook _ComMapClass;
static HRESULT WINAPI _Cache(void* pv, REFIID iid, void** ppvObject, DWORD dw)
{
	_ComMapClass* p = (_ComMapClass*)pv;
	HRESULT hRes = CComObjectRootBase::_Cache(pv, iid, ppvObject, dw);
	return hRes;
}

IUnknown* _GetRawUnknown()
{
	ATLASSERT(_GetEntries()[0].pFunc == _ATL_SIMPLEMAPENTRY);
	return (IUnknown*)((int)this+_GetEntries()->dw);
}

_ATL_DECLARE_GET_UNKNOWN(x)
HRESULT _InternalQueryInterface(REFIID iid, void** ppvObject)
{
	return InternalQueryInterface(this, _GetEntries(), iid, ppvObject);
}
const static _ATL_INTMAP_ENTRY* WINAPI _GetEntries()
{
	static const _ATL_INTMAP_ENTRY _entries[] = { DEBUG_QI_ENTRY(x)
/////

*/
class ATL_NO_VTABLE CDirectDrawSurfaceHook : 
   public CComObjectRootEx<CComMultiThreadModel>,
   public IDirectDrawSurface,
   public IDirectDrawSurface2,
   public IDirectDrawSurface3,
   public IDirectDrawSurface4
{
public:
	CDirectDrawSurfaceHook()
	{
	}

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP_EX(CDirectDrawSurfaceHook)
	COM_INTERFACE_ENTRY_IID( IID_IDirectDrawSurface, IDirectDrawSurface )
	COM_INTERFACE_ENTRY_IID( IID_IDirectDrawSurface2, IDirectDrawSurface2 )
	COM_INTERFACE_ENTRY_IID( IID_IDirectDrawSurface3, IDirectDrawSurface3 )
    COM_INTERFACE_ENTRY_IID( IID_IDirectDrawSurface4, IDirectDrawSurface4 )
END_COM_MAP()

	CComPtr< IDirectDrawSurface > m_pDDS;
	CComPtr< IDirectDrawSurface2 > m_pDDS2;
	CComPtr< IDirectDrawSurface3 > m_pDDS3;
   CComPtr< IDirectDrawSurface4 > m_pDDS4;

   void setObject( IUnknown *pDevice );
   void setSurfaceNum( long nSurfaceNum );
   long m_nSurfaceNum;

public:

	/* IDirectDrawSurface */

	STDMETHOD(AddAttachedSurface)(LPDIRECTDRAWSURFACE p1)
	{
		return m_pDDS->AddAttachedSurface(p1);
	}

    STDMETHOD(AddOverlayDirtyRect)(LPRECT p1)
	{
		return m_pDDS4->AddOverlayDirtyRect(p1);
		//return m_pDDS->AddOverlayDirtyRect(p1);
	}

    STDMETHOD(Blt)(LPRECT p1,LPDIRECTDRAWSURFACE p2, LPRECT p3,DWORD p4, LPDDBLTFX p5)
	{
		return m_pDDS->Blt(p1, p2, p3, p4, p5);
	}

    STDMETHOD(BltBatch)(LPDDBLTBATCH p1, DWORD p2, DWORD p3)
	{
		return m_pDDS4->BltBatch(p1, p2, p3);
		//return m_pDDS->BltBatch(p1, p2, p3);
	}

	STDMETHOD(BltFast)(DWORD p1,DWORD p2,LPDIRECTDRAWSURFACE p3, LPRECT p4,DWORD p5)
	{
		return m_pDDS->BltFast(p1,p2,p3,p4,p5);
	}

    STDMETHOD(DeleteAttachedSurface)(DWORD p1,LPDIRECTDRAWSURFACE p2)
	{
		return m_pDDS->DeleteAttachedSurface(p1,p2);
	}

    STDMETHOD(EnumAttachedSurfaces)(LPVOID p1,LPDDENUMSURFACESCALLBACK p2)
	{
		return m_pDDS->EnumAttachedSurfaces(p1, p2);
	}

    STDMETHOD(EnumOverlayZOrders)(DWORD p1,LPVOID p2,LPDDENUMSURFACESCALLBACK p3)
	{
		return m_pDDS->EnumOverlayZOrders(p1, p2, p3);
	}

    STDMETHOD(Flip)(LPDIRECTDRAWSURFACE p1, DWORD p2)
	{
		return m_pDDS->Flip(p1, p2);
	}
    
	STDMETHOD(GetAttachedSurface)(LPDDSCAPS p1, LPDIRECTDRAWSURFACE FAR *p2)
	{
		return m_pDDS->GetAttachedSurface(p1, p2);
	}

    STDMETHOD(GetBltStatus)(DWORD p1)
	{
		return m_pDDS4->GetBltStatus(p1);
	}	//turn m_pDDS->GetBltStatus(p1);

    STDMETHOD(GetCaps)(LPDDSCAPS p1)
	{
		return m_pDDS->GetCaps(p1);
	}

    STDMETHOD(GetClipper)(LPDIRECTDRAWCLIPPER FAR* p1)
	{
		return m_pDDS4->GetClipper(p1);
		//turn m_pDDS->GetClipper(p1);
	}

    STDMETHOD(GetColorKey)(DWORD p1, LPDDCOLORKEY p2)
	{
		return m_pDDS4->GetColorKey(p1, p2);
		//return m_pDDS->GetColorKey(p1, p2);
	}

    STDMETHOD(GetDC)(HDC FAR *p1)
	{
		//MessageBox(0, "I gota DC", "asd", 0);
		return m_pDDS4->GetDC(p1);
		//return m_pDDS->GetDC(p1);
	}

    STDMETHOD(GetFlipStatus)(DWORD p1)
	{
		return m_pDDS4->GetFlipStatus(p1);
		//return m_pDDS->GetFlipStatus(p1);
	}

    STDMETHOD(GetOverlayPosition)(LPLONG p1, LPLONG p2)
	{
		return m_pDDS4->GetOverlayPosition(p1, p2);
		//return m_pDDS->GetOverlayPosition(p1, p2);
	}

    STDMETHOD(GetPalette)(LPDIRECTDRAWPALETTE FAR* p1)
	{
		return m_pDDS4->GetPalette(p1);
		//return m_pDDS->GetPalette(p1);
	}

    STDMETHOD(GetPixelFormat)(LPDDPIXELFORMAT p1)
	{
		return m_pDDS4->GetPixelFormat(p1);
		//return m_pDDS->GetPixelFormat(p1);
	}

    STDMETHOD(GetSurfaceDesc)(LPDDSURFACEDESC p1)
	{
		return m_pDDS->GetSurfaceDesc(p1);
	}

    STDMETHOD(Initialize)(LPDIRECTDRAW p1, LPDDSURFACEDESC p2)
	{
		return m_pDDS->Initialize(p1, p2);
	}

	STDMETHOD(IsLost)()
	{
		return m_pDDS4->IsLost();
		//return m_pDDS->IsLost();
	}

	STDMETHOD(Lock)(LPRECT p1,LPDDSURFACEDESC p2,DWORD p3,HANDLE p4)
	{
		return m_pDDS->Lock(p1, p2, p3, p4);
	}

	STDMETHOD(ReleaseDC)(HDC p1)
	{
		return m_pDDS4->ReleaseDC(p1);
		//return m_pDDS->ReleaseDC(p1);
	}

	STDMETHOD(Restore)()
    {
		return m_pDDS4->Restore();
		//return m_pDDS->Restore();
	}

	STDMETHOD(SetClipper)(LPDIRECTDRAWCLIPPER p1)
	{
		return m_pDDS4->SetClipper(p1);
		//return m_pDDS->SetClipper(p1);
	}

    STDMETHOD(SetColorKey)(DWORD p1, LPDDCOLORKEY p2)
	{
		return m_pDDS4->SetColorKey(p1, p2);
		//return m_pDDS->SetColorKey(p1, p2);
	}

    STDMETHOD(SetOverlayPosition)(LONG p1, LONG p2)
	{
		return m_pDDS4->SetOverlayPosition(p1, p2);
		//return m_pDDS->SetOverlayPosition(p1, p2);
	}

    STDMETHOD(SetPalette)(LPDIRECTDRAWPALETTE p1)
	{
		return m_pDDS4->SetPalette(p1);
		//return m_pDDS->SetPalette(p1);
	}

    STDMETHOD(Unlock)(LPVOID p1)
	{
		return m_pDDS->Unlock(p1);
	}

    STDMETHOD(UpdateOverlay)(LPRECT p1, LPDIRECTDRAWSURFACE p2,LPRECT p3,DWORD p4, LPDDOVERLAYFX p5)
	{
		return m_pDDS->UpdateOverlay(p1,p2,p3,p4,p5);
	}

    STDMETHOD(UpdateOverlayDisplay)(DWORD p1)
	{
		return m_pDDS4->UpdateOverlayDisplay(p1);
		//return m_pDDS->UpdateOverlayDisplay(p1);
	}

    STDMETHOD(UpdateOverlayZOrder)(DWORD p1, LPDIRECTDRAWSURFACE p2)
	{
		return m_pDDS->UpdateOverlayZOrder(p1, p2);
	}





	/* IDirectDrawSurface2*/

	STDMETHOD(AddAttachedSurface)(LPDIRECTDRAWSURFACE2 p1)
	{
		return m_pDDS2->AddAttachedSurface(p1);
	}
/*
    STDMETHOD(AddOverlayDirtyRect)(LPRECT p1)
    {
		return m_pDDS2->AddOverlayDirtyRect(p1);
	}
	*/
	STDMETHOD(Blt)(LPRECT p1,LPDIRECTDRAWSURFACE2 p2, LPRECT p3,DWORD p4, LPDDBLTFX p5)
	{
		return m_pDDS2->Blt(p1,p2,p3,p4,p5);
	}
    
/*	STDMETHOD(BltBatch)(LPDDBLTBATCH p1, DWORD p2, DWORD p3)
    {
		return m_pDDS2->BltBatch(p1,p2,p3);
	}
*/
	STDMETHOD(BltFast)(DWORD p1,DWORD p2,LPDIRECTDRAWSURFACE2 p3, LPRECT p4,DWORD p5)
    {
		return m_pDDS2->BltFast(p1,p2,p3,p4,p5);
	}

	STDMETHOD(DeleteAttachedSurface)(DWORD p1,LPDIRECTDRAWSURFACE2 p2)
    {
		return m_pDDS2->DeleteAttachedSurface(p1,p2);
	}

/*	STDMETHOD(EnumAttachedSurfaces)(LPVOID p1,LPDDENUMSURFACESCALLBACK p2)
    {
		return m_pDDS2->EnumAttachedSurfaces(p1,p2);
	}
*/
/*	STDMETHOD(EnumOverlayZOrders)(DWORD p1,LPVOID p2,LPDDENUMSURFACESCALLBACK p3)
    {
		return m_pDDS2->EnumOverlayZOrders(p1,p2,p3);
	}
*/
	STDMETHOD(Flip)(LPDIRECTDRAWSURFACE2 p1, DWORD p2)
    {
		return m_pDDS2->Flip(p1, p2);
	}

	STDMETHOD(GetAttachedSurface)(LPDDSCAPS p1, LPDIRECTDRAWSURFACE2 FAR *p2)
    {
		return m_pDDS2->GetAttachedSurface(p1, p2);
	}

/*	STDMETHOD(GetBltStatus)(DWORD p1)
    
		{
		return m_pDDS2->GetBltStatus(p1);
	}
*/
/*	STDMETHOD(GetCaps)(LPDDSCAPS p1)
    {
		return m_pDDS2->GetCaps(p1);
	}
*/
/*	STDMETHOD(GetClipper)(LPDIRECTDRAWCLIPPER FAR* p1)
    {
		return m_pDDS2->GetClipper(p1);
	}
*/
/*	STDMETHOD(GetColorKey)(DWORD p1, LPDDCOLORKEY p2)
    {
		return m_pDDS2->GetColorKey(p1, p2);
	}
*/
/*	STDMETHOD(GetDC)(HDC FAR *p1)
    {
		return m_pDDS2->GetDC(p1);
	}
*/
/*	STDMETHOD(GetFlipStatus)(DWORD p1)
    {
		return m_pDDS2->GetFlipStatus(p1);
	}
*/
/*	STDMETHOD(GetOverlayPosition)(LPLONG p1, LPLONG p2)
    {
		return m_pDDS2->GetOverlayPosition(p1,p2);
	}
*/
/*	STDMETHOD(GetPalette)(LPDIRECTDRAWPALETTE FAR* p1)
    {
		return m_pDDS2->GetPalette(p1);
	}
*/
/*	STDMETHOD(GetPixelFormat)(LPDDPIXELFORMAT p1)
    {
		return m_pDDS2->GetPixelFormat(p1);
	}
*/
/*	STDMETHOD(GetSurfaceDesc)(LPDDSURFACEDESC p1)
    {
		return m_pDDS2->GetSurfaceDesc(p1);
	}
*/
/*	STDMETHOD(Initialize)(LPDIRECTDRAW p1, LPDDSURFACEDESC p2)
    {
		return m_pDDS2->Initialize(p1, p2);
	}
*/
/*	STDMETHOD(IsLost)()
    {
		return m_pDDS2->IsLost();
	}
*/
/*	STDMETHOD(Lock)(LPRECT p1,LPDDSURFACEDESC p2,DWORD p3,HANDLE p4)
    {
		return m_pDDS2->Lock(p1, p2, p3, p4);
	}
*/
/*	STDMETHOD(ReleaseDC)(HDC p1)
    {
		return m_pDDS2->ReleaseDC(p1);
	}
*/
/*	STDMETHOD(Restore)()
    {
		return m_pDDS2->Restore();
	}
*/
/*	STDMETHOD(SetClipper)(LPDIRECTDRAWCLIPPER p1)
    {
		return m_pDDS2->SetClipper(p1);
	}
*/
/*	STDMETHOD(SetColorKey)(DWORD p1, LPDDCOLORKEY p2)
    {
		return m_pDDS2->SetColorKey(p1, p2);
	}
*/
/*	STDMETHOD(SetOverlayPosition)(LONG p1, LONG p2)
    {
		return m_pDDS2->SetOverlayPosition(p1, p2);
	}
*/
/*	STDMETHOD(SetPalette)(LPDIRECTDRAWPALETTE p1)
    {
		return m_pDDS2->SetPalette(p1);
	}
*/
/*	STDMETHOD(Unlock)(LPVOID p1)
    {
		return m_pDDS2->Unlock(p1);
	}
*/
	STDMETHOD(UpdateOverlay)(LPRECT p1, LPDIRECTDRAWSURFACE2 p2,LPRECT p3,DWORD p4, LPDDOVERLAYFX p5)
    {
		return m_pDDS2->UpdateOverlay(p1, p2, p3, p4, p5);
	}

/*	STDMETHOD(UpdateOverlayDisplay)(DWORD p1)
    {
		return m_pDDS2->UpdateOverlayDisplay(p1);
	}
*/
	STDMETHOD(UpdateOverlayZOrder)(DWORD p1, LPDIRECTDRAWSURFACE2 p2)
	{
		return m_pDDS2->UpdateOverlayZOrder(p1, p2);
	}

    STDMETHOD(GetDDInterface)(LPVOID FAR *p1)
    {
		return m_pDDS4->GetDDInterface(p1);
		//return m_pDDS2->GetDDInterface(p1);
	}

	STDMETHOD(PageLock)(DWORD p1)
    {
		return m_pDDS4->PageLock(p1);
		//return m_pDDS2->PageLock(p1);
	}

	STDMETHOD(PageUnlock)(DWORD p1)
	{
		return m_pDDS4->PageUnlock(p1);
		//return m_pDDS2->PageUnlock(p1);
	}


	/* IDirectDrawSurface3 */

    STDMETHOD(AddAttachedSurface)(LPDIRECTDRAWSURFACE3 p1)
	{
		return m_pDDS3->AddAttachedSurface(p1);
	}

/*    STDMETHOD(AddOverlayDirtyRect)(LPRECT p1)
    {
		return m_pDDS3->AddOverlayDirtyRect(p1);
	}
*/	
	STDMETHOD(Blt)(LPRECT p1,LPDIRECTDRAWSURFACE3 p2, LPRECT p3,DWORD p4, LPDDBLTFX p5)
    {
		return m_pDDS3->Blt(p1,p2,p3,p4,p5);
	}
	
/*	STDMETHOD(BltBatch)(LPDDBLTBATCH p1, DWORD p2, DWORD p3)
    {
		return m_pDDS3->BltBatch(p1, p2, p3);
	}
*/	
	STDMETHOD(BltFast)(DWORD p1,DWORD p2,LPDIRECTDRAWSURFACE3 p3, LPRECT p4,DWORD p5)
    {
		return m_pDDS3->BltFast(p1,p2,p3,p4,p5);
	}
	
	STDMETHOD(DeleteAttachedSurface)(DWORD p1,LPDIRECTDRAWSURFACE3 p2)
    {
		return m_pDDS3->DeleteAttachedSurface(p1,p2);
	}
	
/*	STDMETHOD(EnumAttachedSurfaces)(LPVOID p1,LPDDENUMSURFACESCALLBACK p2)
    {
		return m_pDDS3->EnumAttachedSurfaces(p1, p2);
	}
*/	
/*	STDMETHOD(EnumOverlayZOrders)(DWORD p1,LPVOID p2,LPDDENUMSURFACESCALLBACK p3)
    {
		return m_pDDS3->EnumOverlayZOrders(p1,p2,p3);
	}
*/	
	STDMETHOD(Flip)(LPDIRECTDRAWSURFACE3 p1, DWORD p2)
    {
		return m_pDDS3->Flip(p1,p2);
	}
	
	STDMETHOD(GetAttachedSurface)(LPDDSCAPS p1, LPDIRECTDRAWSURFACE3 FAR *p2)
    {
		return m_pDDS3->GetAttachedSurface(p1,p2);
	}
	
/*	STDMETHOD(GetBltStatus)(DWORD p1)
    {
		return m_pDDS3->GetBltStatus(p1);
	}
*/	
/*	STDMETHOD(GetCaps)(LPDDSCAPS p1)
    {
		return m_pDDS3->GetCaps(p1);
	}
*/
/*	STDMETHOD(GetClipper)(LPDIRECTDRAWCLIPPER FAR* p1)
    {
		return m_pDDS3->GetClipper(p1);
	}
*/
/*	STDMETHOD(GetColorKey)(DWORD p1, LPDDCOLORKEY p2)
    {
		return m_pDDS3->GetColorKey(p1, p2);
	}
*/
/*	STDMETHOD(GetDC)(HDC FAR *p1)
    {
		return m_pDDS3->GetDC(p1);
	}
*/	
/*	STDMETHOD(GetFlipStatus)(DWORD p1)
    {
		return m_pDDS3->GetFlipStatus(p1);
	}
*/	
/*	STDMETHOD(GetOverlayPosition)(LPLONG p1, LPLONG p2)
    {
		return m_pDDS3->GetOverlayPosition(p1, p2);
	}
*/	
/*	STDMETHOD(GetPalette)(LPDIRECTDRAWPALETTE FAR* p1)
    {
		return m_pDDS3->GetPalette(p1);
	}
*/	
/*	STDMETHOD(GetPixelFormat)(LPDDPIXELFORMAT p1)
    {
		return m_pDDS3->GetPixelFormat(p1);
	}
*/	
/*	STDMETHOD(GetSurfaceDesc)(LPDDSURFACEDESC p1)
    {
		return m_pDDS3->GetSurfaceDesc(p1);
	}
*/	
/*	STDMETHOD(Initialize)(LPDIRECTDRAW p1, LPDDSURFACEDESC p2)
    {
		return m_pDDS3->Initialize(p1, p2);
	}
*/	
/*	STDMETHOD(IsLost)()
    {
		return m_pDDS3->IsLost();
	}
*/	
/*	STDMETHOD(Lock)(LPRECT p1,LPDDSURFACEDESC p2,DWORD p3,HANDLE p4)
    {
		return m_pDDS3->Lock(p1, p2, p3, p4);
	}
*/	
/*	STDMETHOD(ReleaseDC)(HDC p1)
    {
		return m_pDDS3->ReleaseDC(p1);
	}
*/	
/*	STDMETHOD(Restore)()
    {
		return m_pDDS3->Restore();
	}
*/	
/*	STDMETHOD(SetClipper)(LPDIRECTDRAWCLIPPER p1)
    {
		return m_pDDS3->SetClipper(p1);
	}
*/	
/*	STDMETHOD(SetColorKey)(DWORD p1, LPDDCOLORKEY p2)
    {
		return m_pDDS3->SetColorKey(p1, p2);
	}
*/	
/*	STDMETHOD(SetOverlayPosition)(LONG p1, LONG p2)
    {
		return m_pDDS3->SetOverlayPosition(p1,p2);
	}
*/	
/*	STDMETHOD(SetPalette)(LPDIRECTDRAWPALETTE p1)
    {
		return m_pDDS3->SetPalette(p1);
	}
*/	
/*	STDMETHOD(Unlock)(LPVOID p1)
    {
		return m_pDDS3->Unlock(p1);
	}
*/	
	STDMETHOD(UpdateOverlay)(LPRECT p1, LPDIRECTDRAWSURFACE3 p2, LPRECT p3,DWORD p4, LPDDOVERLAYFX p5)
    {
		return m_pDDS3->UpdateOverlay(p1,p2,p3,p4,p5);
	}
	
/*	STDMETHOD(UpdateOverlayDisplay)(DWORD p1)
    {
		return m_pDDS3->UpdateOverlayDisplay(p1);
	}
*/	
	STDMETHOD(UpdateOverlayZOrder)(DWORD p1, LPDIRECTDRAWSURFACE3 p2)
    {
		return m_pDDS3->UpdateOverlayZOrder(p1, p2);
	}
	
/*    STDMETHOD(GetDDInterface)(LPVOID FAR *p1)
    {
		return m_pDDS3->GetDDInterface(p1);
	}
*/	
/*	STDMETHOD(PageLock)(DWORD p1)
    {
		return m_pDDS3->PageLock(p1);
	}
*/	
/*	STDMETHOD(PageUnlock)(DWORD p1)
    {
		return m_pDDS3->PageUnlock(p1);
	}
*/	
    STDMETHOD(SetSurfaceDesc)(LPDDSURFACEDESC p1, DWORD p2)
	{
		return m_pDDS3->SetSurfaceDesc(p1, p2);
	}






	
	/* IDirectDrawSurface4 */
    STDMETHOD(AddAttachedSurface)(LPDIRECTDRAWSURFACE4 p1)
	{
		return m_pDDS4->AddAttachedSurface(p1);
	}

/*    STDMETHOD(AddOverlayDirtyRect)(LPRECT p1)
	{
		return m_pDDS4->AddOverlayDirtyRect(p1);
	}
*/
    STDMETHOD(Blt)(LPRECT p1, LPDIRECTDRAWSURFACE4 p2, LPRECT p3, DWORD p4, LPDDBLTFX p5);

/*    STDMETHOD(BltBatch)( LPDDBLTBATCH p1, DWORD p2, DWORD p3) 
	{
		return m_pDDS4->BltBatch(p1, p2, p3);
	}
*/
    STDMETHOD(BltFast)( DWORD p1,DWORD p2,LPDIRECTDRAWSURFACE4 p3, LPRECT p4,DWORD p5) 
	{
		MessageBeep(0);
		return m_pDDS4->BltFast(p1, p2, p3, p4, p5);
	}

    STDMETHOD(DeleteAttachedSurface)( DWORD p1,LPDIRECTDRAWSURFACE4 p2) 
	{
		return m_pDDS4->DeleteAttachedSurface(p1, p2);
	}

    STDMETHOD(EnumAttachedSurfaces)( LPVOID p1,LPDDENUMSURFACESCALLBACK2 p2) 
	{
		return m_pDDS4->EnumAttachedSurfaces(p1, p2);
	}

    STDMETHOD(EnumOverlayZOrders)( DWORD p1,LPVOID p2,LPDDENUMSURFACESCALLBACK2 p3) 
	{
		return m_pDDS4->EnumOverlayZOrders(p1, p2, p3);
	}

    STDMETHOD(Flip)( LPDIRECTDRAWSURFACE4 p1, DWORD p2);

	STDMETHOD(GetAttachedSurface)( LPDDSCAPS2 p1, LPDIRECTDRAWSURFACE4 FAR *p2) 
	{
		return m_pDDS4->GetAttachedSurface(p1, p2);
	}

/*    STDMETHOD(GetBltStatus)( DWORD p1) 
	{
		return m_pDDS4->GetBltStatus(p1);
	}
*/
    STDMETHOD(GetCaps)( LPDDSCAPS2 p1) 
	{
		return m_pDDS4->GetCaps(p1);
	}

/*	STDMETHOD(GetClipper)( LPDIRECTDRAWCLIPPER FAR* p1) 
	{
		return m_pDDS4->GetClipper(p1);
	}
*/
/*    STDMETHOD(GetColorKey)( DWORD p1, LPDDCOLORKEY p2) 
	{
		return m_pDDS4->GetColorKey(p1, p2);
	}
*/
/*    STDMETHOD(GetDC)( HDC FAR *p1) 
	{
		return m_pDDS4->GetDC(p1);
	}
*/
/*    STDMETHOD(GetFlipStatus)( DWORD p1) 
	{
		return m_pDDS4->GetFlipStatus(p1);
	}
*/
/*    STDMETHOD(GetOverlayPosition)( LPLONG p1, LPLONG p2) 
	{
		return m_pDDS4->GetOverlayPosition(p1, p2);
	}
*/
/*    STDMETHOD(GetPalette)( LPDIRECTDRAWPALETTE FAR* p1) 
	{
		return m_pDDS4->GetPalette(p1);
	}
*/
/*    STDMETHOD(GetPixelFormat)( LPDDPIXELFORMAT p1) 
	{
		return m_pDDS4->GetPixelFormat(p1);
	}
*/
    STDMETHOD(GetSurfaceDesc)( LPDDSURFACEDESC2 p1) 
	{
		return m_pDDS4->GetSurfaceDesc(p1);
	}

    STDMETHOD(Initialize)( LPDIRECTDRAW p1, LPDDSURFACEDESC2 p2) 
	{
		return m_pDDS4->Initialize(p1, p2);
	}

/*    STDMETHOD(IsLost)() 
	{
		return m_pDDS4->IsLost();
	}
*/
    STDMETHOD(Lock)(LPRECT p1,LPDDSURFACEDESC2 p2,DWORD p3,HANDLE p4) 
	{
		return m_pDDS4->Lock(p1, p2, p3, p4);
	}

/*    STDMETHOD(ReleaseDC)( HDC p1) 
	{
		return m_pDDS4->ReleaseDC(p1);
	}
*/
/*    STDMETHOD(Restore)() 
	{
		return m_pDDS4->Restore();
	}
*/
/*    STDMETHOD(SetClipper)( LPDIRECTDRAWCLIPPER p1) 
	{
		return m_pDDS4->SetClipper(p1);
	}
*/
/*    STDMETHOD(SetColorKey)( DWORD p1, LPDDCOLORKEY p2) 
	{
		return m_pDDS4->SetColorKey(p1, p2);
	}
*/
/*    STDMETHOD(SetOverlayPosition)( LONG p1, LONG p2) 
	{
		return m_pDDS4->SetOverlayPosition(p1, p2);
	}
*/
/*    STDMETHOD(SetPalette)( LPDIRECTDRAWPALETTE p1) 
	{
		return m_pDDS4->SetPalette(p1);
	}
*/
    STDMETHOD(Unlock)(LPRECT p1) 
	{
		return m_pDDS4->Unlock(p1);
	}

    STDMETHOD(UpdateOverlay)( LPRECT p1, LPDIRECTDRAWSURFACE4 p2,LPRECT p3,DWORD p4, LPDDOVERLAYFX p5) 
	{
		return m_pDDS4->UpdateOverlay(p1, p2, p3, p4, p5);
	}

/*    STDMETHOD(UpdateOverlayDisplay)( DWORD p1) 
	{
		return m_pDDS4->UpdateOverlayDisplay(p1);
	}
*/
    STDMETHOD(UpdateOverlayZOrder)( DWORD p1, LPDIRECTDRAWSURFACE4 p2) 
	{
		return m_pDDS4->UpdateOverlayZOrder(p1, p2);
	}

/*    STDMETHOD(GetDDInterface)( LPVOID FAR *p1) 
	{
		return m_pDDS4->GetDDInterface(p1);
	}
*/
/*    STDMETHOD(PageLock)( DWORD p1) 
	{
		return m_pDDS4->PageLock(p1);
	}
*/
/*    STDMETHOD(PageUnlock)( DWORD p1) 
	{
		return m_pDDS4->PageUnlock(p1);
	}
*/
    STDMETHOD(SetSurfaceDesc)( LPDDSURFACEDESC2 p1, DWORD p2) 
	{
		return m_pDDS4->SetSurfaceDesc(p1, p2);
	}

    STDMETHOD(SetPrivateData)( REFGUID p1, LPVOID p2, DWORD p3, DWORD p4) 
	{
		return m_pDDS4->SetPrivateData(p1, p2, p3, p4);
	}

    STDMETHOD(GetPrivateData)( REFGUID p1, LPVOID p2, LPDWORD p3) 
	{
		return m_pDDS4->GetPrivateData(p1, p2, p3);
	}

    STDMETHOD(FreePrivateData)( REFGUID p1) 
	{
		return m_pDDS4->FreePrivateData(p1);
	}

    STDMETHOD(GetUniquenessValue)( LPDWORD p1) 
	{
		return m_pDDS4->GetUniquenessValue(p1);
	}

    STDMETHOD(ChangeUniquenessValue)() 
	{
		return m_pDDS4->ChangeUniquenessValue();
	}

};

#endif //__DIRECTDRAWSURFACEHOOK_H_

