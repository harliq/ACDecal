// ACHooks.h: Definition of the ACHooks class

#ifndef __ACHOOKS_H_
#define __ACHOOKS_H_


#include "resource.h"       // main symbols
#include "Decal.h"
#include "DecalManager.h"
#include "DecalCP.h"

// the public key to unencrypt xmls
#include "..\include\DecalKey.h"

// .NET vs VC6.0 Compiler Config 
#if _MSC_VER > 1200 // .NET 
#import "..\Include\Inject.tlb" 
#define IPSite DecalPlugins::IPluginSite 
#else // Not .Net 
#include "..\inject\Inject.h"
#define IPSite IPluginSite 
#endif 

struct sMemoryLocation
{
	std::string Name;
	long Location;	
};

// qString struct for internal client strings - this is all cynica_l's work
struct qString
{
	void **vTable;
	char *Data;
	long MaximumLength;
};

struct qSkill {   
	long vTable;   
	enum eTrainLevel Trained;   
	int TotalXP;   
	int FreePoints;   
	int Clicks;   
	int unk1;   
	int unk2; /* float? percent of total? */   
	float unk3;   
};   

struct qVital {   
	long vTable;   
	int Clicks;   
	int unk1;   
	int TotalXP;   
	int unk2;   
};   

struct qAttribute {   
	long vTable;   
	int Clicks;   
	int Start;   
	int TotalXP;   
}; 

struct qPointerList
{
	qPointerList *dd[1];
};

extern "C" bool DispatchChatMessage( char *pText, long *pdwColor );
extern "C" bool DispatchChatText( char *pText );

/////////////////////////////////////////////////////////////////////////////
// ACHooks
class ATL_NO_VTABLE cACHooks : 
	public CComObjectRoot,
	public CComCoClass<cACHooks,&CLSID_ACHooks>,
	public IConnectionPointContainerImpl<cACHooks>,
	public IDispatchImpl<IACHooks, &IID_IACHooks, &LIBID_Decal>,
	public CProxyIACHooksEvents< cACHooks >
{
public:
	cACHooks();
	~cACHooks();

DECLARE_REGISTRY_RESOURCEID(IDR_ACHooks)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cACHooks)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IACHooks)
	COM_INTERFACE_ENTRY_IMPL(IConnectionPointContainer)
END_COM_MAP()

BEGIN_CONNECTION_POINT_MAP(cACHooks)
	CONNECTION_POINT_ENTRY(DIID_IACHooksEvents)
END_CONNECTION_POINT_MAP()

private:
	long*	GetCs(long offs=0) ;
	void	DecryptXML( const char *szPath, std::string &szXML );

	long	m_Hooks ;
	typedef std::map< std::string, sMemoryLocation > LocMap;
	LocMap m_mLocationList;

	bool m_bDecalRef;
	bool m_bIdQueueRef;

	bool m_bMismatch;

	bool m_bPrevSelect;
	long m_lPrevSelect[2];

	bool m_bCurrentSelect;
	long m_lCurrentSelect[2];

	bool m_bChatMessageAddy;
	long m_lChatMessageAddy;

	bool m_bMouse;
	long m_lMouse[4];

	bool m_bCastSpell;
	long m_lCastSpell;

	bool m_bMoveItem;
	long m_lMoveItem;

	bool m_bSelectItem;
	long m_lSelectItem;

	bool m_bUseItem;
	long m_lUseItem;

	bool m_bAllYourBase;
	long m_lAllYourBase;
	bool m_bSetCombatState;
	long m_lSetCombatState;
	bool m_bCombatStateOffset;
	long m_lCombatStateOffset;

	bool m_bChatState;
	long m_lChatState;

	bool m_bRequestID;
	long m_lRequestID;

	bool m_bStackCount;
	long m_lStackCount;

	bool m_bTestFormula;
	long m_lTestFormula;
	long m_lTestFormulaVTable;

	bool m_bVendorID;
	long m_lVendorID;

	bool m_bBusyState;
	long m_lBusyState;
	bool m_bBusyStateID;
	long m_lBusyStateID;

	bool m_bPointerState;
	long m_lPointerState;

	bool m_bMoveItemEx;
	long m_lMoveItemEx;

	bool m_bArea3DWidth;
	long *m_lpArea3DWidth;

	bool m_bArea3DHeight;
	long *m_lpArea3DHeight;

	bool m_bObjectFromGuid;
	long m_lObjectFromGuid;
	long m_lObjectFromGuidClass;

	bool m_bFaceHeading;

	long m_lMovementThingyParent;
	long m_lMovementThingyOffset;
	long m_lFaceHeading;

	bool m_bSetAutorun;
	long m_lSetAutorun;

	bool m_bInternalStringConstructor;
	bool m_bInternalStringDestructor;
	bool m_bSendMessageToID;
	bool m_bSendMessageToName;
	bool m_bSendMessageToMask;
	bool m_bLocalChatText;
	bool m_bLocalChatEmote;

	long m_lInternalStringConstructor;
	long m_lInternalStringDestructor;
	long m_lSendMessageToID;
	long m_lSendMessageToName;
	long m_lSendMessageToMask;
	long m_lLocalChatText;
	long m_lLocalChatEmote;

	long m_lVitalBase;

	bool m_bGetVital;
	long m_lGetVital;

	bool m_bGetAttribute;
	long m_lGetAttribute;

	bool m_bGetSkill;
	long m_lGetSkill;

	bool m_bLogout;
	long m_lLogout;

	bool m_bSecureTrade_Add;
	long m_lSecureTrade_Add;
	bool m_bSecureTrade_Add_Off1;
	long m_lSecureTrade_Add_Off1;
	bool m_bSecureTrade_Add_Off2;
	long m_lSecureTrade_Add_Off2;

	bool m_bGetSkillInfo;
	long m_lGetSkillInfo, m_lGetSkillInfo_vT;
	bool m_bGetAttributeInfo;
	long m_lGetAttributeInfo, m_lGetAttributeInfo_vT;
	bool m_bGetVitalInfo;
	long m_lGetVitalInfo, m_lGetVitalInfo_vT;

	bool m_bChatColor;
	long m_lChatColor;

	bool m_bSelectItemHook;
	long m_lSelectItemHook;

	bool m_bUstAddItem_Useable;
	long m_lUstAddItem;
	long m_lUstAddItem_Off1;
	long m_lUstAddItem_Off2;

	bool m_bRequestShortcircuit;
	long m_lRequestShortcircuit1;
	long m_lRequestShortcircuit2;
	long m_lRequestShortcircuit3;

	bool m_bToolTextHook;
	long m_lToolTextHJ;
	bool m_bToolText2Hook;
	long m_lToolText2HJ;

	long m_lIdleLoc;
	bool m_bIdleLoc;

	long m_lSlashDay;
	bool m_bSlashDay;

	unsigned int m_HooksEx[1];
	unsigned int m_HookCount;

  CComPtr< IDecal > m_pDecal;

public:
	static cACHooks* s_pACHooks;
	void InternalObjectDestroyed( DWORD dwGuid );
	void InternalSelectItem( DWORD dwID );
	void InternalShortcircuit( DWORD dwID );
	bool InternalChatText( char *szText );
	bool InternalChatMessage( char *szText, long lColor );
	void InternalToolText( char *szText, VARIANT_BOOL bError );
	void InternalToolTextAppend( char *szText, VARIANT_BOOL bError );

	void SetHookEx(enum eAvailableHooksEx HookID);
	CComPtr< IKitchenSink > m_pIdQueue;

	STDMETHOD(MoveItemEx)(long lObjectID, long lDestinationID);
	STDMETHOD(get_PointerState)(long *pVal);
	STDMETHOD(get_BusyStateID)(long *pVal);
	STDMETHOD(get_BusyState)(long *pVal);
	STDMETHOD(get_VendorID)(long *pVal);
	STDMETHOD(get_SelectedStackCount)(long lStackCount, long *pVal);
	STDMETHOD(put_SelectedStackCount)(long lStackCount, long newVal);
	STDMETHOD(GetFellowStats)(long lCharID);
	STDMETHOD(UseItemEx)(long UseThis, long OnThis);
	STDMETHOD(get_ChatState)(VARIANT_BOOL *pVal);
	STDMETHOD(get_CombatState)(long *pVal);
	STDMETHOD(SetCombatState)(long pVal);
	STDMETHOD(UseItem)(long lObjectID, long lUseState);
	STDMETHOD(UseItemRaw)(long lObjectID, long lUseState, long lUseMethod);
	STDMETHOD(UseFociSpell)(long UseThis, long OnThis);
	STDMETHOD(SelectItem)(long lObjectID);
	STDMETHOD(MoveItem)(long lObjectID, long lPackID, long lSlot, VARIANT_BOOL bStack);
	STDMETHOD(CastSpell)(long lSpellID, long lObjectID);
	STDMETHOD(SetCursorPosition)(long lX, long lY);
	STDMETHOD(RawChatOut)(BSTR szText, long lColor);
	STDMETHOD(ChatOut)(BSTR szText, long lColor);
	STDMETHOD(get_PreviousSelection)(long *pVal);
	STDMETHOD(put_PreviousSelection)(long newVal);
	STDMETHOD(get_CurrentSelection)(long *pVal);
	STDMETHOD(put_CurrentSelection)(long newVal);
	STDMETHOD(QueryMemLoc)(BSTR bstrName, long *pVal);
	STDMETHOD(get_Heading)(double *pVal);
	STDMETHOD(get_Landblock)(long *pVal);
	STDMETHOD(get_LocationX)(double *pVal);
	STDMETHOD(get_LocationY)(double *pVal);
	STDMETHOD(get_LocationZ)(double *pVal);
	STDMETHOD(DropItem)(long lObjectID);
	STDMETHOD(get_HooksAvail)(long *pVal) ;
	STDMETHOD(FaceHeading)(float fHeading, VARIANT_BOOL bUnknown, VARIANT_BOOL *pRetval);
	STDMETHOD(get_Area3DWidth)(long *pVal);
	STDMETHOD(get_Area3DHeight)(long *pVal);
	STDMETHOD(ItemIsKnown)(long lGUID, VARIANT_BOOL* pRetval) ;
	STDMETHOD(SendTell)(long lPlayerID, BSTR Message);
	STDMETHOD(SendTellEx)(BSTR Name, BSTR Message);
	STDMETHOD(SetAutorun)(VARIANT_BOOL bOnOff) ;
	STDMETHOD(get_Vital)(long Vital, long* pVal);
	STDMETHOD(get_Attribute)(long Attribute, long* pVal);
	STDMETHOD(get_Skill)(long Skill, long* pVal);
	STDMETHOD(LocalChatText)(BSTR Text);
	STDMETHOD(LocalChatEmote)(BSTR EmoteText);
	STDMETHOD(get_HooksAvailEx)(enum eAvailableHooksEx HookID, VARIANT_BOOL* pVal);
	STDMETHOD(Logout)();
	STDMETHOD(ToolText)(BSTR Text, VARIANT_BOOL bError);
	STDMETHOD(ToolTextAppend)(BSTR Text, VARIANT_BOOL bError);
	STDMETHOD(SetIdleTime)( double dIdleTimeout );
	STDMETHOD(SetDecal)(IUnknown *pDecal);

	STDMETHOD(SecureTrade_Add)(long ItemID, VARIANT_BOOL *pVal);   

	STDMETHOD(get_SkillTrainLevel)( eSkill SkillID, eTrainLevel *pVal );   
	STDMETHOD(get_SkillTotalXP)( eSkill SkillID, int *pVal );   
	STDMETHOD(get_SkillFreePoints)( eSkill SkillID, int *pVal );   
	STDMETHOD(get_SkillClicks)( eSkill SkillID, int *pVal );   

	STDMETHOD(get_AttributeClicks)( eAttribute AttributeID, int *pVal );   
	STDMETHOD(get_AttributeTotalXP)( eAttribute AttributeID, int *pVal );   
	STDMETHOD(get_AttributeStart)( eAttribute AttributeID, int *pVal );   

	STDMETHOD(get_VitalClicks)( eVital VitalID, int *pVal );   
	STDMETHOD(get_VitalTotalXP)( eVital VitalID, int *pVal ); 

	HRESULT GetSkillInfo(eSkill SkillID, struct qSkill *Skill);   
	HRESULT GetAttributeInfo(eAttribute AttributeID, struct qAttribute *Attribute);   
	HRESULT GetVitalInfo(eVital VitalID, struct qVital *Vital); 
	STDMETHOD(RequestID)(long lObjectID);
	STDMETHOD(IDQueueAdd)(long lObjectID);
	STDMETHOD(SetIDFilter)(IKitchenSink* pIDFilter);
	STDMETHOD(UstAddItem)(long lObjectID);
	STDMETHOD(SendMessageByMask)(LONG lMask, BSTR szMessage);
	STDMETHOD(SetDay)(VARIANT_BOOL bDay);
};

#endif // __ACHOOKS_H_
