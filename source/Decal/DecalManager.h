// DecalManager.h : Declaration of the cDecal

#ifndef __DECAL_H_
#define __DECAL_H_

#include "resource.h"       // main symbols

class cPluginSite;

/////////////////////////////////////////////////////////////////////////////
// cDecal
class ATL_NO_VTABLE cDecal : 
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<cDecal, &CLSID_Decal>,
	public IDecal
{
public:
	cDecal()
      : m_pD( NULL ),
      m_pD3D( NULL ),
      m_hWnd( NULL ),
      m_bFocus( VARIANT_FALSE ),
      m_bServicesStarted( false ),
      m_bPluginsStarted( false )
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_DECAL)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cDecal)
COM_INTERFACE_ENTRY(IDecal)
END_COM_MAP()

   struct cService
   {
      CLSID m_clsid;
      DWORD m_dwCaps;
      CComPtr< IDecalService > m_p;
   };

   enum eServiceCaps
   {
      eServiceRender = 0x01
   };

   typedef std::vector< cService > cServiceList;
   cServiceList m_services;

   struct cPlugin
   {
      CLSID m_clsid;
      cPluginSite *m_pSite;
   };

   typedef std::vector< cPlugin > cPluginList;
   cPluginList m_plugins;

   bool m_bServicesStarted,
      m_bPluginsStarted;

   IDirectDraw4 *m_pD;
   IDirect3DDevice3 *m_pD3D;
   HWND m_hWnd;
   VARIANT_BOOL m_bFocus;
   CComPtr< IACHooks > m_pHooks;

   HRESULT convertToken( std::string &strToken, std::string &strOut );

public:
// IDecal
   STDMETHOD(InitGraphics)( IUnknown *pDirectDraw, IUnknown *pD3DDevice );
	STDMETHOD(get_DirectDraw)(REFIID iid, VOID * * ppvItf);
	STDMETHOD(get_D3DDevice)(REFIID iid, VOID * * ppvItf);
	STDMETHOD(Render3D)();
	STDMETHOD(Render2D)();

	STDMETHOD(get_HWND)(LONG * pVal);
    STDMETHOD(put_HWND)(LONG newVal);

	STDMETHOD(get_Focus)(VARIANT_BOOL *pVal);
    STDMETHOD(put_Focus)(VARIANT_BOOL newVal);

	STDMETHOD(get_ScreenSize)(long *pWidth, long *pHeight);
	STDMETHOD(get_Object)(BSTR strPath, REFIID iid, /*[out, retval]*/ LPVOID *pVal);
	STDMETHOD(MapPath)(BSTR pPath, BSTR * pMapped);

	STDMETHOD(StartServices)();
	STDMETHOD(StopServices)();
	STDMETHOD(StartPlugins)();
	STDMETHOD(StopPlugins)();
	STDMETHOD(get_Configuration)(BSTR strType, /*[default(GUID_NULL)]*/ REFCLSID clsidAdvance, /*[out, retval]*/ IDecalEnum * *pVal);
	STDMETHOD(get_Service)(REFCLSID clsid, REFIID iid, /*[out, retval]*/ LPVOID *pVal);
	STDMETHOD(get_Plugin)(REFCLSID clsid, REFIID iid, /*[out, retval]*/ LPVOID *pVal);
	STDMETHOD(get_Hooks)(IACHooks** pVal);
};

#endif //__DECAL_H_
