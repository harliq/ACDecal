
Decalps.dll: dlldata.obj Decal_p.obj Decal_i.obj
	link /dll /out:Decalps.dll /def:Decalps.def /entry:DllMain dlldata.obj Decal_p.obj Decal_i.obj \
		kernel32.lib rpcndr.lib rpcns4.lib rpcrt4.lib oleaut32.lib uuid.lib \

.c.obj:
	cl /c /Ox /DWIN32 /D_WIN32_WINNT=0x0400 /DREGISTER_PROXY_DLL \
		$<

clean:
	@del Decalps.dll
	@del Decalps.lib
	@del Decalps.exp
	@del dlldata.obj
	@del Decal_p.obj
	@del Decal_i.obj
