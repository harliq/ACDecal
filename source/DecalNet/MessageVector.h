// MessageVector.h : Declaration of the cMessageVector

#ifndef __MESSAGEVECTOR_H_
#define __MESSAGEVECTOR_H_

#include "resource.h"       // main symbols
#include "MessageImpl.h"

/////////////////////////////////////////////////////////////////////////////
// cMessageVector
class ATL_NO_VTABLE cMessageVectorIter : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public IDispatchImpl<IMessageMember, &IID_IMessageMember, &LIBID_DecalNet>,
	public IMessageIteratorSublistImpl
{
public:
	cMessageVectorIter()
	{
	}

BEGIN_COM_MAP(cMessageVectorIter)
   COM_INTERFACE_ENTRY(IMessageMember)
	COM_INTERFACE_ENTRY(IMessageIterator)
	COM_INTERFACE_ENTRY2(IDispatch, IMessageMember)
END_COM_MAP()

public:
   // IMessageIterator Overrides
   STDMETHOD(get_MemberName)(BSTR *pVal);

   // IMessageMember Implementation
	STDMETHOD(get_Count)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_Member)(VARIANT vIndex, /*[out, retval]*/ VARIANT *pVal);
	STDMETHOD(get_MemberName)(long Index, BSTR *pVal);
};

#endif //__MESSAGEVECTOR_H_
