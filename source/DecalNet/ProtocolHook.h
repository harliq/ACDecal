#pragma once

#include "ACMessage.h"

class HookedMessage : public ACMessage
{
public:
	HookedMessage (BYTE *pData, DWORD dwSize)
	{
		_ASSERTE (pData != NULL);
		_ASSERTE (m_dwSize >= 4);

		m_pData = pData;
		m_dwSize = dwSize;
	}

	virtual BYTE *getData ()
	{
		_ASSERTE (m_pData != NULL);
		return m_pData;
	}

	virtual DWORD getSize ()
	{
		return m_dwSize;
	}

	virtual DWORD getType ()
	{
		_ASSERTE (m_pData);
		_ASSERTE (m_dwSize >= sizeof (DWORD));

		if (m_pData && m_dwSize >= sizeof (DWORD))
		{
			return * (DWORD *) m_pData;
		}

		return 0;
	}

protected:
	LPBYTE m_pData;
	DWORD m_dwSize;
};
