// DecalNetImpl.h
// Declaration of class to help you implement Decal Network Filters

#ifndef __DECALNETIMPL_H
#define __DECALNETIMPL_H

#include <DecalNet.h>

// The solo network filter does not have any depencies and does not store
// the INetServices object - clients must implement INetworkFilter::Dispatch
template< class ImplT >
class ATL_NO_VTABLE ISoloNetworkFilterImpl
: public INetworkFilter2
{
public:
   STDMETHOD(Initialize)(INetService *)
   {
      return S_OK;
   }

   STDMETHOD(Terminate)()
   {
      return S_OK;
   }

   STDMETHOD(DispatchServer)(IMessage2 *)
   {
      return S_OK;
   }

   STDMETHOD(DispatchClient)(IMessage2 *)
   {
      return S_OK;
   }
};

// This version stores the INetService pointer passed during initialization.
// A network can use this to recall back to the Decal object and get extended
// state information
template< class ImplT >
class ATL_NO_VTABLE INetworkFilterImpl
: public INetworkFilter2
{
public:
   CComPtr< INetService > m_pService;

   HRESULT onInitialize()
   {
      return S_OK;
   }

   void onTerminate()
   {
   }

   STDMETHOD(Initialize)(INetService *pService)
   {
      m_pService = pService;

      HRESULT hRes = static_cast< ImplT * >( this )->onInitialize();

      if( FAILED( hRes ) )
         m_pService.Release();

      return hRes;
   }

   STDMETHOD(Terminate)()
   {
      static_cast< ImplT * >( this )->onTerminate();
      m_pService.Release();

      return S_OK;
   }

   STDMETHOD(DispatchServer)(IMessage2 *)
   {
      return S_OK;
   }

   STDMETHOD(DispatchClient)(IMessage2 *)
   {
      return S_OK;
   }
};

#endif
