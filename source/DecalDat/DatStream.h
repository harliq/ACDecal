// DatStream.h : Declaration of the cDatStream

#ifndef __DATSTREAM_H_
#define __DATSTREAM_H_

#include "resource.h"       // main symbols

#include "DatFile.h"

/////////////////////////////////////////////////////////////////////////////
// cDatStream
class ATL_NO_VTABLE cDatStream : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<cDatStream, &CLSID_DatStream>,
	public IDatStream
{
public:
	cDatStream()
	{
	}

   cDatFile::cFile *m_pFile;

DECLARE_REGISTRY_RESOURCEID(IDR_DATSTREAM)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cDatStream)
	COM_INTERFACE_ENTRY(IDatStream)
END_COM_MAP()

// IDatStream
public:
	STDMETHOD(Read)(long Bytes, /*[out, retval]*/ BSTR *Data);
	STDMETHOD(ReadBinary)(long Bytes, /*[size_is(Bytes)]*/ BYTE *Buffer);
	STDMETHOD(Restart)();
	STDMETHOD(Skip)(long Bytes);
	STDMETHOD(get_Tell)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_Size)(/*[out, retval]*/ long *pVal);
};

#endif //__DATSTREAM_H_
