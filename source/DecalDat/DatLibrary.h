// DatLibrary.h : Declaration of the cDatLibrary

#ifndef __DATLIBRARY_H_
#define __DATLIBRARY_H_

#include "resource.h"       // main symbols

class cDatFile;
class cDatService;

/////////////////////////////////////////////////////////////////////////////
// cDatLibrary
class ATL_NO_VTABLE cDatLibrary : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<cDatLibrary, &CLSID_DatLibrary>,
   public IDecalDirectory,
	public IDispatchImpl<IDatLibrary, &IID_IDatLibrary, &LIBID_DecalDat>
{
public:
	cDatLibrary()
      : m_pFile( NULL )
	{
	}

   void load( cDatService *pService, BSTR strFilename, int nFileType, long nSectorSize );

   int m_nFileType;
   cDatFile *m_pFile;
   cDatService *m_pService;

   HRESULT createFile( DWORD dwFile, REFIID iid, void **ppvItf );

DECLARE_REGISTRY_RESOURCEID(IDR_DATLIBRARY)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cDatLibrary)
	COM_INTERFACE_ENTRY(IDatLibrary)
   COM_INTERFACE_ENTRY(IDecalDirectory)
	COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()

// IDatLibrary
public:

   // IDecalDirectory
   STDMETHOD(Lookup)(BSTR strName, IUnknown **ppItf);
	STDMETHOD(Open)(BSTR Protocol, DWORD File, /*[out, retval]*/ LPUNKNOWN *pFile);
	STDMETHOD(get_Stream)(DWORD dwType, /*[out, retval]*/ LPUNKNOWN *pVal);
};

#endif //__DATLIBRARY_H_
