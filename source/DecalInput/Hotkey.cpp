// Hotkey.cpp : Implementation of cHotkey
#include "stdafx.h"
#include "DecalInput.h"
#include "Hotkey.h"

#include "InputService.h"

/////////////////////////////////////////////////////////////////////////////
// cHotkey

STDMETHODIMP cHotkey::get_Tag(VARIANT *pVal)
{
   if( pVal == NULL )
   {
      _ASSERT( FALSE );
      return E_POINTER;
   }

   return ::VariantCopy( pVal, &m_tag );
}

STDMETHODIMP cHotkey::put_Tag(VARIANT newVal)
{
   m_tag = newVal;

	return S_OK;
}

STDMETHODIMP cHotkey::get_Key(BSTR *pVal)
{
   if( pVal == NULL )
   {
      _ASSERT( FALSE );
      return E_POINTER;
   }

   USES_CONVERSION;

   if( m_nVK == -1 )
   {
      // Key must be set before it's retreived
      _ASSERT( FALSE );
      return E_FAIL;
   }

   // Try and convert it to a special char
   cInputService::cCharNames *i_char = cInputService::charFromVK( m_nVK );
   if( i_char != cInputService::end_chars() )
   {
      TCHAR szKey[ 12 ];
      ::_stprintf( szKey, _T( "{%s}" ), i_char->szName );

      *pVal = T2BSTR( szKey );
      return S_OK;
   }

   TCHAR szKey[2] = { static_cast< TCHAR >( m_nVK ), _T( '\0' ) };
   *pVal = T2BSTR( szKey );

	return S_OK;
}

STDMETHODIMP cHotkey::put_Key(BSTR newVal)
{
   USES_CONVERSION;

   // Convert to upper case
   LPTSTR szKey = OLE2T( newVal );
   ::_tcsupr( szKey );

   if( szKey[ 0 ] == '{' )
   {
      for( LPTSTR szEndKey = szKey + 1; *szEndKey != '\0'; ++ szEndKey )
      {
         if( *szEndKey == _T( '}' ) )
         {
            cInputService::cCharNames *i_char = cInputService::charFromName( szKey + 1, szEndKey - ( szKey + 1 ) );
            if( i_char == cInputService::end_chars() )
            {
               // Unknown special character name
               _ASSERT( FALSE );
               return E_INVALIDARG;
            }

            // We have our vkey
            m_nVK = i_char->m_nVKey;
            return S_OK;
         }
      }

      // Malformed string
      _ASSERT( FALSE );
      return E_INVALIDARG;
   }

   if( szKey[ 1 ] != _T( '\0' ) )
   {
      // Malformed string - should only contain one character
      _ASSERT( FALSE );
      return E_INVALIDARG;
   }

   m_nVK = szKey[ 0 ];

	return S_OK;
}

STDMETHODIMP cHotkey::get_Enabled(VARIANT_BOOL *pVal)
{
   if( pVal == NULL )
   {
      _ASSERT( FALSE );
      return E_POINTER;
   }

   *pVal = ( m_bEnabled ) ? VARIANT_TRUE : VARIANT_FALSE;

	return S_OK;
}

STDMETHODIMP cHotkey::put_Enabled(VARIANT_BOOL newVal)
{
   if( !!newVal == m_bEnabled )
      return S_FALSE;

   if( cInputService::g_p == NULL )
   {
      // Input Service must be started to enable or disable a hotkey
      _ASSERT( FALSE );
      return E_FAIL;
   }

   m_bEnabled = !!newVal;

   if( m_bEnabled )
   {
#ifdef _DEBUG
      for( cInputService::cHotkeyList::iterator i = cInputService::g_p->m_hotkeys.begin(); i != cInputService::g_p->m_hotkeys.end(); ++ i )
         _ASSERTE( *i != this );
#endif

      cInputService::g_p->m_hotkeys.push_back( this );
   }
   else
   {
      for( cInputService::cHotkeyList::iterator i = cInputService::g_p->m_hotkeys.begin(); i != cInputService::g_p->m_hotkeys.end(); ++ i )
      {
         if( *i == this )
         {
            cInputService::g_p->m_hotkeys.erase( i );
            break;
         }
      }

      _ASSERTE( i != cInputService::g_p->m_hotkeys.end() );
   }

	return S_OK;
}
