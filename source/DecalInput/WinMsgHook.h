// WinMsgHook.h : Declaration of the cWinMsgHook

#ifndef __WINMSGHOOK_H_
#define __WINMSGHOOK_H_

#include "resource.h"       // main symbols
#include "DecalInputCP.h"

/////////////////////////////////////////////////////////////////////////////
// cWinMsgHook
class ATL_NO_VTABLE cWinMsgHook : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<cWinMsgHook, &CLSID_WinMsgHook>,
   public IProvideClassInfo2Impl< &CLSID_WinMsgHook, &DIID_IWinMsgHookEvents, &LIBID_DecalInput >,
	public IConnectionPointContainerImpl<cWinMsgHook>,
	public IDispatchImpl<IWinMsgHook, &IID_IWinMsgHook, &LIBID_DecalInput>,
	public CProxyIWinMsgHookEvents< cWinMsgHook >
{
public:
	cWinMsgHook()
      : m_bEnabled( false )
	{
	}

   void FinalRelease()
   {
      if( m_bEnabled )
         put_Enabled( VARIANT_FALSE );
   }

   _variant_t m_tag;
   bool m_bEnabled;

DECLARE_REGISTRY_RESOURCEID(IDR_WINMSGHOOK)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cWinMsgHook)
	COM_INTERFACE_ENTRY(IWinMsgHook)
	COM_INTERFACE_ENTRY(IDispatch)
   COM_INTERFACE_ENTRY(IProvideClassInfo)
   COM_INTERFACE_ENTRY(IProvideClassInfo2)
	COM_INTERFACE_ENTRY_IMPL(IConnectionPointContainer)
END_COM_MAP()
BEGIN_CONNECTION_POINT_MAP(cWinMsgHook)
CONNECTION_POINT_ENTRY(DIID_IWinMsgHookEvents)
END_CONNECTION_POINT_MAP()

// IWinMsgHook
public:
	STDMETHOD(get_Enabled)(/*[out, retval]*/ VARIANT_BOOL *pVal);
	STDMETHOD(put_Enabled)(/*[in]*/ VARIANT_BOOL newVal);
	STDMETHOD(get_Tag)(/*[out, retval]*/ VARIANT *pVal);
	STDMETHOD(put_Tag)(/*[in]*/ VARIANT newVal);
};

#endif //__WINMSGHOOK_H_
