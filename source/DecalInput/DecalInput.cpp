// DecalInput.cpp : Implementation of DLL Exports.


// Note: Proxy/Stub Information
//      To build a separate proxy/stub DLL, 
//      run nmake -f DecalInputps.mk in the project directory.

#include "stdafx.h"
#include "resource.h"
#include <initguid.h>
#include "DecalInput.h"

#include "DecalInput_i.c"
#include "InputService.h"
#include "Timer.h"
#include "Hotkey.h"
#include "WinMsgHook.h"
#include "InputBuffer.h"
#include "TypeAction.h"
#include "MouseMoveAction.h"
#include "DelayAction.h"
#include "EventAction.h"
#include "PolledDelayAction.h"
#include "RestoreAction.h"

CComModule _Module;

BEGIN_OBJECT_MAP(ObjectMap)
OBJECT_ENTRY(CLSID_InputService, cInputService)
OBJECT_ENTRY(CLSID_Timer, cTimer)
OBJECT_ENTRY(CLSID_Hotkey, cHotkey)
OBJECT_ENTRY(CLSID_WinMsgHook, cWinMsgHook)
OBJECT_ENTRY(CLSID_InputBuffer, cInputBuffer)
OBJECT_ENTRY(CLSID_TypeAction, cTypeAction)
OBJECT_ENTRY(CLSID_MouseMoveAction, cMouseMoveAction)
OBJECT_ENTRY(CLSID_DelayAction, cDelayAction)
OBJECT_ENTRY(CLSID_EventAction, cEventAction)
OBJECT_ENTRY(CLSID_PolledDelayAction, cPolledDelayAction)
OBJECT_ENTRY(CLSID_RestoreAction, cRestoreAction)
END_OBJECT_MAP()

/////////////////////////////////////////////////////////////////////////////
// DLL Entry Point

extern "C"
BOOL WINAPI DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID /*lpReserved*/)
{
    if (dwReason == DLL_PROCESS_ATTACH)
    {
        _Module.Init(ObjectMap, hInstance, &LIBID_DecalInput);
        DisableThreadLibraryCalls(hInstance);
    }
    else if (dwReason == DLL_PROCESS_DETACH)
        _Module.Term();
    return TRUE;    // ok
}

/////////////////////////////////////////////////////////////////////////////
// Used to determine whether the DLL can be unloaded by OLE

STDAPI DllCanUnloadNow(void)
{
    return (_Module.GetLockCount()==0) ? S_OK : S_FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// Returns a class factory to create an object of the requested type

STDAPI DllGetClassObject(REFCLSID rclsid, REFIID riid, LPVOID* ppv)
{
    return _Module.GetClassObject(rclsid, riid, ppv);
}

/////////////////////////////////////////////////////////////////////////////
// DllRegisterServer - Adds entries to the system registry

STDAPI DllRegisterServer(void)
{
    // registers object, typelib and all interfaces in typelib
    return _Module.RegisterServer(TRUE);
}

/////////////////////////////////////////////////////////////////////////////
// DllUnregisterServer - Removes entries from the system registry

STDAPI DllUnregisterServer(void)
{
    return _Module.UnregisterServer(TRUE);
}


