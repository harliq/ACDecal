// DelayAction.h : Declaration of the cDelayAction

#ifndef __DELAYACTION_H_
#define __DELAYACTION_H_

#include "resource.h"       // main symbols
#include <DecalInputImpl.h>

/////////////////////////////////////////////////////////////////////////////
// cDelayAction
class ATL_NO_VTABLE cDelayAction : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<cDelayAction, &CLSID_DelayAction>,
	public IInputActionImpl< cDelayAction >
{
public:
	cDelayAction()
	{
	}

   long m_nTime;

   HRESULT onLoad( LPTSTR szData )
   {
      // Attempt to convert it to a number
      return ( ::_stscanf( szData, _T( "%i" ), &m_nTime ) == 1 ) ? S_OK : E_INVALIDARG;
   }

DECLARE_REGISTRY_RESOURCEID(IDR_DELAYACTION)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cDelayAction)
	COM_INTERFACE_ENTRY(IInputAction)
END_COM_MAP()

// IInputAction
public:
   STDMETHOD(Execute)()
   {
      m_pSite->Delay( m_nTime, VARIANT_TRUE );
      return S_OK;
   }
};

#endif //__DELAYACTION_H_
