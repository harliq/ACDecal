// PolledDelayAction.h : Declaration of the cPolledDelayAction

#ifndef __POLLEDDELAYACTION_H_
#define __POLLEDDELAYACTION_H_

#include "resource.h"       // main symbols
#include <DecalInputImpl.h>

/////////////////////////////////////////////////////////////////////////////
// cPolledDelayAction
class ATL_NO_VTABLE cPolledDelayAction : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<cPolledDelayAction, &CLSID_PolledDelayAction>,
	public IInputActionImpl< cPolledDelayAction >
{
public:
	cPolledDelayAction()
      : m_bFirst( true )
	{
	}

   bool m_bFirst;
   long m_nTime,
      m_nEvent;

   HRESULT load( LPTSTR szBuffer )
   {
      int nSep = ::_tcscspn( szBuffer, _T( ";" ) );
      if( nSep == -1 )
         return E_INVALIDARG;

      szBuffer[ nSep ] = _T( '\0' );
      if( ::_stscanf( szBuffer, _T( "%i" ), &m_nTime ) != 1 )
         return E_INVALIDARG;

      if( ::_stscanf( szBuffer + nSep + 1, _T( "%i" ), &m_nEvent ) != 1 )
         return E_INVALIDARG;

      return S_OK;
   }

DECLARE_REGISTRY_RESOURCEID(IDR_POLLEDDELAYACTION)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cPolledDelayAction)
	COM_INTERFACE_ENTRY(IInputAction)
END_COM_MAP()

// IPolledDelayAction
public:
   STDMETHOD(Reset)()
   {
      m_bFirst = true;
      return S_OK;
   }

   STDMETHOD(Execute)()
   {
      if( !m_bFirst )
      {
         // Fire the event to figure out if we should continue
         VARIANT_BOOL bContinue = VARIANT_FALSE;
         VARIANT vParam;
         vParam.vt = VT_BOOL | VT_BYREF;
         vParam.pboolVal = &bContinue;
         
         m_pSite->FireEvent( m_nEvent, vParam );

         if( !bContinue )
            // Abort and let it carry on
            return S_OK;
      }
      else
         // Clear the first flag
         m_bFirst = false;

      m_pSite->Delay( m_nTime, VARIANT_FALSE );

      return S_OK;
   }
};

#endif //__POLLEDDELAYACTION_H_
