//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by DecalInput.rc
//
#define IDS_PROJNAME                    100
#define IDR_INPUTSERVICE                101
#define IDR_TIMER                       102
#define IDR_HOTKEY                      103
#define IDR_WINMSGHOOK                  104
#define IDR_WNDMSG                      105
#define IDR_INPUTBUFFER                 106
#define IDR_TYPEACTION                  107
#define IDR_MOUSEMOVEACTION             108
#define IDR_DELAYACTION                 109
#define IDR_EVENTACTION                 110
#define IDR_POLLEDDELAYACTION           111
#define IDR_RESTOREACTION               112

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        201
#define _APS_NEXT_COMMAND_VALUE         32768
#define _APS_NEXT_CONTROL_VALUE         201
#define _APS_NEXT_SYMED_VALUE           113
#endif
#endif
