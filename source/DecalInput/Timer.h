// Timer.h : Declaration of the cTimer

#ifndef __TIMER_H_
#define __TIMER_H_

#include "resource.h"       // main symbols
#include "DecalInputCP.h"

/////////////////////////////////////////////////////////////////////////////
// cTimer
class ATL_NO_VTABLE cTimer : 
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<cTimer, &CLSID_Timer>,
	public IConnectionPointContainerImpl<cTimer>,
   public IProvideClassInfo2Impl< &CLSID_Timer, &DIID_ITimerEvents, &LIBID_DecalInput >,
	public IDispatchImpl<IDecalTimer, &IID_IDecalTimer, &LIBID_DecalInput>,
	public CProxyITimerEvents< cTimer >
{
public:
	cTimer()
      : m_bStarted( false )
	{
	}

   void FinalRelease()
   {
      if( m_bStarted )
         Stop();
   }

   _variant_t m_tag;
   bool m_bStarted;
   long m_nStart,
      m_nInterval;

DECLARE_REGISTRY_RESOURCEID(IDR_TIMER)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cTimer)
	COM_INTERFACE_ENTRY(IDecalTimer)
	COM_INTERFACE_ENTRY(IDispatch)
   COM_INTERFACE_ENTRY(IProvideClassInfo)
   COM_INTERFACE_ENTRY(IProvideClassInfo2)
	COM_INTERFACE_ENTRY_IMPL(IConnectionPointContainer)
END_COM_MAP()
BEGIN_CONNECTION_POINT_MAP(cTimer)
CONNECTION_POINT_ENTRY(DIID_ITimerEvents)
END_CONNECTION_POINT_MAP()

// ITimer
public:
	STDMETHOD(get_Running)(/*[out, retval]*/ VARIANT_BOOL *pVal);
	STDMETHOD(get_Tag)(/*[out, retval]*/ VARIANT *pVal);
	STDMETHOD(put_Tag)(/*[in]*/ VARIANT newVal);
	STDMETHOD(Stop)();
	STDMETHOD(Start)(long Interval);
};

#endif //__TIMER_H_
