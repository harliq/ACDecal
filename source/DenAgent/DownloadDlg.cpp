// DownloadDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DenAgent.h"
#include "DownloadDlg.h"

#include "URLCallback.h"
#include "BindStatusCallback.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// cDownloadDlg dialog


cDownloadDlg::cDownloadDlg(CWnd* pParent /*=NULL*/)
	: CDialog(cDownloadDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(cDownloadDlg)
	//}}AFX_DATA_INIT
}


void cDownloadDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(cDownloadDlg)
	DDX_Control(pDX, IDC_STATUSTEXT, m_stIEMsg);
	DDX_Control(pDX, IDC_CUSTOMSTATUS, m_stCustomMsg);
	DDX_Control(pDX, IDC_STOPDOWNLOAD, m_wndStop);
	DDX_Control(pDX, IDC_PROGRESS, m_wndProgress);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(cDownloadDlg, CDialog)
	//{{AFX_MSG_MAP(cDownloadDlg)
	ON_BN_CLICKED(IDC_STOPDOWNLOAD, OnStopdownload)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// cDownloadDlg message handlers

void cDownloadDlg::OnStopdownload() 
{
	m_pCallback->stop();
}

BOOL cDownloadDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	// Create the URL Bind Context
	CComObject< cURLCallback > *pCallback;
	CComObject< cURLCallback >::CreateInstance( &pCallback );
	m_pCallback = pCallback;
	m_pCallback->AddRef();
	m_pCallback->m_pDlg = this;
	m_pCallback->start( m_clsid, m_strURL, m_dwMajor, m_dwMinor );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void cDownloadDlg::OnDestroy() 
{	
	m_pCallback->Release();

	CDialog::OnDestroy();
}