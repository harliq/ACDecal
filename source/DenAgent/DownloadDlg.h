#if !defined(AFX_DOWNLOADDLG_H__E5094CC4_4B01_46AF_9C32_5DBECF5C11D6__INCLUDED_)
#define AFX_DOWNLOADDLG_H__E5094CC4_4B01_46AF_9C32_5DBECF5C11D6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DownloadDlg.h : header file
//

#include "resource.h"

class cURLCallback;

/////////////////////////////////////////////////////////////////////////////
// cDownloadDlg dialog

class cDownloadDlg : public CDialog
{
// Construction
public:
	cDownloadDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(cDownloadDlg)
	enum { IDD = IDD_DOWNLOAD };
	CStatic	m_stIEMsg;
	CStatic	m_stCustomMsg;
	CButton	m_wndStop;
	CProgressCtrl	m_wndProgress;
	//}}AFX_DATA

   cURLCallback *m_pCallback;

   CLSID m_clsid;
   LPCWSTR m_strURL;
   DWORD m_dwMajor;
   DWORD m_dwMinor;
   CComPtr< IClassFactory > m_pFactory;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(cDownloadDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(cDownloadDlg)
	afx_msg void OnStopdownload();
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DOWNLOADDLG_H__E5094CC4_4B01_46AF_9C32_5DBECF5C11D6__INCLUDED_)
