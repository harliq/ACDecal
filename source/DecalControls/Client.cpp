// Client.cpp : Implementation of cClient
#include "stdafx.h"
#include "DecalControls.h"
#include "Client.h"

/////////////////////////////////////////////////////////////////////////////
// cClient

STDMETHODIMP cClient::LayerCreate( ILayerSite *pSite )
{
   m_pSite = pSite;

   return S_OK;
}

STDMETHODIMP cClient::LayerDestroy()
{
   m_pSite.Release();

   return S_OK;
}

STDMETHODIMP cClient::SchemaLoad(IView *pView, IUnknown *pXMLSchema)
{
   MSXML::IXMLDOMElementPtr pElement = pXMLSchema;

   long nID = 1000;

   // Create all child controls from schema and let them be
   // placed wherever they like
   MSXML::IXMLDOMElementPtr pChild;
   for( MSXML::IXMLDOMNodeListPtr pChildren = pElement->selectNodes( _T( "control" ) );
         ( pChild = pChildren->nextNode() ).GetInterfacePtr() != NULL; )
      pView->LoadControl( m_pSite, nID ++, pChild );

   return S_OK;
}
