// Static.h : Declaration of the cStatic

#ifndef __STATIC_H_
#define __STATIC_H_

#include "resource.h"       // main symbols
#include "DecalControlsCP.h"

#include "SinkImpl.h"

/////////////////////////////////////////////////////////////////////////////
// cStatic
class ATL_NO_VTABLE cStatic : 
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<cStatic, &CLSID_StaticText>,
   public ILayerImpl< cStatic >,
   public IControlImpl< cStatic, IStatic, &IID_IStatic, &LIBID_DecalControls >,
   public ILayerRenderImpl,
   public ILayerSchema,
   public IProvideClassInfo2Impl< &CLSID_StaticText, &DIID_IControlEvents, &LIBID_DecalControls >,
	public IConnectionPointContainerImpl<cStatic>,
	public CProxyIControlEvents< cStatic >
{
public:
	cStatic()
    : m_nTextColor( RGB( 0, 0, 0 ) )
	{
	}

	void DrawText(LPPOINT ppt, BSTR szText, ICanvas *pCanvas);

   CComPtr< IFontCache > m_pFont;
   _bstr_t m_strText;
   long m_nTextColor;
   long m_nOutlineColor;
   bool m_bAA;
   bool m_bOutline;
   //long m_nWidth;
   //long m_nHeight;
   eFontJustify m_eJustify;

DECLARE_REGISTRY_RESOURCEID(IDR_STATIC)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cStatic)
   COM_INTERFACE_ENTRY(ILayerRender)
   COM_INTERFACE_ENTRY(ILayer)
   COM_INTERFACE_ENTRY(ILayerSchema)
   COM_INTERFACE_ENTRY(IDispatch)
   COM_INTERFACE_ENTRY(IControl)
	COM_INTERFACE_ENTRY(IStatic)
   COM_INTERFACE_ENTRY(IProvideClassInfo)
   COM_INTERFACE_ENTRY(IProvideClassInfo2)
	COM_INTERFACE_ENTRY_IMPL(IConnectionPointContainer)
END_COM_MAP()

BEGIN_CONNECTION_POINT_MAP(cStatic)
CONNECTION_POINT_ENTRY(DIID_IControlEvents)
END_CONNECTION_POINT_MAP()

public:
   // IStatic Methods
	STDMETHOD(get_Text)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_Text)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_Font)(/*[out, retval]*/ IFontCacheDisp * *pVal);
	STDMETHOD(putref_Font)(/*[in]*/ IFontCacheDisp * newVal);
	STDMETHOD(get_TextColor)(/*[out, retval]*/ long *pVal);
	STDMETHOD(put_TextColor)(/*[in]*/ long newVal);

   // ILayerSchema Methods
   STDMETHOD(SchemaLoad)(IView *pView, IUnknown *pSchema);

   // ILayerRender Methods
   STDMETHOD(Render)(ICanvas *);
};

#endif //__STATIC_H_
