
DecalControlsps.dll: dlldata.obj DecalControls_p.obj DecalControls_i.obj
	link /dll /out:DecalControlsps.dll /def:DecalControlsps.def /entry:DllMain dlldata.obj DecalControls_p.obj DecalControls_i.obj \
		kernel32.lib rpcndr.lib rpcns4.lib rpcrt4.lib oleaut32.lib uuid.lib \

.c.obj:
	cl /c /Ox /DWIN32 /D_WIN32_WINNT=0x0400 /DREGISTER_PROXY_DLL \
		$<

clean:
	@del DecalControlsps.dll
	@del DecalControlsps.lib
	@del DecalControlsps.exp
	@del dlldata.obj
	@del DecalControls_p.obj
	@del DecalControls_i.obj
