#ifndef _DECALCONTROLSCP_H_
#define _DECALCONTROLSCP_H_

template <class T>
class CProxyISliderEvents : public IConnectionPointImpl<T, &DIID_ISliderEvents, CComDynamicUnkArray>
{
	//Warning this class may be recreated by the wizard.
public:
	HRESULT Fire_Destroy(LONG nID)
	{
		try
		{
			CComVariant varResult;
			T* pT = static_cast<T*>(this);
			int nConnectionIndex;
			CComVariant* pvars = new CComVariant[1];
			int nConnections = m_vec.GetSize();
			
			for (nConnectionIndex = 0; nConnectionIndex < nConnections; nConnectionIndex++)
			{
				pT->Lock();
				CComPtr<IUnknown> sp = m_vec.GetAt(nConnectionIndex);
				pT->Unlock();
				IDispatch* pDispatch = reinterpret_cast<IDispatch*>(sp.p);
				if (pDispatch != NULL)
				{
					VariantClear(&varResult);
					pvars[0] = nID;
					DISPPARAMS disp = { pvars, NULL, 1, 0 };
					pDispatch->Invoke(0x1, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &disp, &varResult, NULL, NULL);
				}
			}
			delete[] pvars;
			return varResult.scode;
		}
		catch (...)
		{
			// In debug mode warn the user
			_ASSERTE( FALSE );

			// Return failure
			return E_FAIL;
		}
	}

	HRESULT Fire_Change(LONG nID, LONG nValue)
	{
		try
		{
			CComVariant varResult;
			T* pT = static_cast<T*>(this);
			int nConnectionIndex;
			CComVariant* pvars = new CComVariant[2];
			int nConnections = m_vec.GetSize();
			
			for (nConnectionIndex = 0; nConnectionIndex < nConnections; nConnectionIndex++)
			{
				pT->Lock();
				CComPtr<IUnknown> sp = m_vec.GetAt(nConnectionIndex);
				pT->Unlock();
				IDispatch* pDispatch = reinterpret_cast<IDispatch*>(sp.p);
				if (pDispatch != NULL)
				{
					VariantClear(&varResult);
					pvars[1] = nID;
					pvars[0] = nValue;
					DISPPARAMS disp = { pvars, NULL, 2, 0 };
					pDispatch->Invoke(0x6, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &disp, &varResult, NULL, NULL);
				}
			}
			delete[] pvars;
			return varResult.scode;
		}
		catch (...)
		{
			// In debug mode warn the user
			_ASSERTE( FALSE );

			// Return failure
			return E_FAIL;
		}
	}
};


template <class T>
class CProxyICheckboxEvents : public IConnectionPointImpl<T, &DIID_ICheckboxEvents, CComDynamicUnkArray>
{
	//Warning this class may be recreated by the wizard.
public:
	HRESULT Fire_Destroy(LONG nID)
	{
		try
		{
			CComVariant varResult;
			T* pT = static_cast<T*>(this);
			int nConnectionIndex;
			CComVariant* pvars = new CComVariant[1];
			int nConnections = m_vec.GetSize();
			
			for (nConnectionIndex = 0; nConnectionIndex < nConnections; nConnectionIndex++)
			{
				pT->Lock();
				CComPtr<IUnknown> sp = m_vec.GetAt(nConnectionIndex);
				pT->Unlock();
				IDispatch* pDispatch = reinterpret_cast<IDispatch*>(sp.p);
				if (pDispatch != NULL)
				{
					VariantClear(&varResult);
					pvars[0] = nID;
					DISPPARAMS disp = { pvars, NULL, 1, 0 };
					pDispatch->Invoke(0x1, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &disp, &varResult, NULL, NULL);
				}
			}
			delete[] pvars;
			return varResult.scode;
		}
		catch (...)
		{
			// In debug mode warn the user
			_ASSERTE( FALSE );

			// Return failure
			return E_FAIL;
		}
	}

	HRESULT Fire_Change(LONG nID, VARIANT_BOOL bChecked)
	{
		try
		{
			CComVariant varResult;
			T* pT = static_cast<T*>(this);
			int nConnectionIndex;
			CComVariant* pvars = new CComVariant[2];
			int nConnections = m_vec.GetSize();
			
			for (nConnectionIndex = 0; nConnectionIndex < nConnections; nConnectionIndex++)
			{
				pT->Lock();
				CComPtr<IUnknown> sp = m_vec.GetAt(nConnectionIndex);
				pT->Unlock();
				IDispatch* pDispatch = reinterpret_cast<IDispatch*>(sp.p);
				if (pDispatch != NULL)
				{
					VariantClear(&varResult);
					pvars[1] = nID;
					pvars[0] = bChecked;
					DISPPARAMS disp = { pvars, NULL, 2, 0 };
					pDispatch->Invoke(0x6, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &disp, &varResult, NULL, NULL);
				}
			}
			delete[] pvars;
			return varResult.scode;
		}
		catch (...)
		{
			// In debug mode warn the user
			_ASSERTE( FALSE );

			// Return failure
			return E_FAIL;
		}
	}
};


template <class T>
class CProxyIChoiceEvents : public IConnectionPointImpl<T, &DIID_IChoiceEvents, CComDynamicUnkArray>
{
	//Warning this class may be recreated by the wizard.
public:
	VOID Fire_Destroy(LONG nID)
	{
		try
		{
			T* pT = static_cast<T*>(this);
			int nConnectionIndex;
			CComVariant* pvars = new CComVariant[1];
			int nConnections = m_vec.GetSize();
			
			for (nConnectionIndex = 0; nConnectionIndex < nConnections; nConnectionIndex++)
			{
				pT->Lock();
				CComPtr<IUnknown> sp = m_vec.GetAt(nConnectionIndex);
				pT->Unlock();
				IDispatch* pDispatch = reinterpret_cast<IDispatch*>(sp.p);
				if (pDispatch != NULL)
				{
					pvars[0] = nID;
					DISPPARAMS disp = { pvars, NULL, 1, 0 };
					pDispatch->Invoke(0x1, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &disp, NULL, NULL, NULL);
				}
			}
			delete[] pvars;
		}
		catch (...)
		{
			// In debug mode warn the user
			_ASSERTE( FALSE );
		}
	}

	VOID Fire_Change(LONG nID, LONG nIndex)
	{
		try
		{
			T* pT = static_cast<T*>(this);
			int nConnectionIndex;
			CComVariant* pvars = new CComVariant[2];
			int nConnections = m_vec.GetSize();
			
			for (nConnectionIndex = 0; nConnectionIndex < nConnections; nConnectionIndex++)
			{
				pT->Lock();
				CComPtr<IUnknown> sp = m_vec.GetAt(nConnectionIndex);
				pT->Unlock();
				IDispatch* pDispatch = reinterpret_cast<IDispatch*>(sp.p);
				if (pDispatch != NULL)
				{
					pvars[1] = nID;
					pvars[0] = nIndex;
					DISPPARAMS disp = { pvars, NULL, 2, 0 };
					pDispatch->Invoke(0x6, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &disp, NULL, NULL, NULL);
				}
			}
			delete[] pvars;
		}
		catch (...)
		{
			// In debug mode warn the user
			_ASSERTE( FALSE );
		}
	}

	VOID Fire_DropDown(LONG nID)
	{
		try
		{
			T* pT = static_cast<T*>(this);
			int nConnectionIndex;
			CComVariant* pvars = new CComVariant[1];
			int nConnections = m_vec.GetSize();
			
			for (nConnectionIndex = 0; nConnectionIndex < nConnections; nConnectionIndex++)
			{
				pT->Lock();
				CComPtr<IUnknown> sp = m_vec.GetAt(nConnectionIndex);
				pT->Unlock();
				IDispatch* pDispatch = reinterpret_cast<IDispatch*>(sp.p);
				if (pDispatch != NULL)
				{
					pvars[0] = nID;
					DISPPARAMS disp = { pvars, NULL, 1, 0 };
					pDispatch->Invoke(0xc, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &disp, NULL, NULL, NULL);
				}
			}
			delete[] pvars;
		}
		catch (...)
		{
			// In debug mode warn the user
			_ASSERTE( FALSE );
		}
	}
};



template <class T>
class CProxyIListEvents : public IConnectionPointImpl<T, &DIID_IListEvents, CComDynamicUnkArray>
{
	//Warning this class may be recreated by the wizard.
public:
	VOID Fire_Destroy(LONG nID)
	{
		try
		{
			T* pT = static_cast<T*>(this);
			int nConnectionIndex;
			CComVariant* pvars = new CComVariant[1];
			int nConnections = m_vec.GetSize();
			
			for (nConnectionIndex = 0; nConnectionIndex < nConnections; nConnectionIndex++)
			{
				pT->Lock();
				CComPtr<IUnknown> sp = m_vec.GetAt(nConnectionIndex);
				pT->Unlock();
				IDispatch* pDispatch = reinterpret_cast<IDispatch*>(sp.p);
				if (pDispatch != NULL)
				{
					pvars[0] = nID;
					DISPPARAMS disp = { pvars, NULL, 1, 0 };
					pDispatch->Invoke(0x1, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &disp, NULL, NULL, NULL);
				}
			}
			delete[] pvars;
		}
		catch (...)
		{
			// In debug mode warn the user
			_ASSERTE( FALSE );
		}
	}

	VOID Fire_Change(LONG nID, LONG nX, LONG nY)
	{
		try
		{
			T* pT = static_cast<T*>(this);
			int nConnectionIndex;
			CComVariant* pvars = new CComVariant[3];
			int nConnections = m_vec.GetSize();
			
			for (nConnectionIndex = 0; nConnectionIndex < nConnections; nConnectionIndex++)
			{
				pT->Lock();
				CComPtr<IUnknown> sp = m_vec.GetAt(nConnectionIndex);
				pT->Unlock();
				IDispatch* pDispatch = reinterpret_cast<IDispatch*>(sp.p);
				if (pDispatch != NULL)
				{
					pvars[2] = nID;
					pvars[1] = nX;
					pvars[0] = nY;
					DISPPARAMS disp = { pvars, NULL, 3, 0 };
					pDispatch->Invoke(0x6, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &disp, NULL, NULL, NULL);
				}
			}
			delete[] pvars;
		}
		catch (...)
		{
			// In debug mode warn the user
			_ASSERTE( FALSE );
		}
	}
};

template <class T>
class CProxyINotebookEvents : public IConnectionPointImpl<T, &DIID_INotebookEvents, CComDynamicUnkArray>
{
	//Warning this class may be recreated by the wizard.
public:
	VOID Fire_Destroy(LONG nID)
	{
		try
		{
			T* pT = static_cast<T*>(this);
			int nConnectionIndex;
			CComVariant* pvars = new CComVariant[1];
			int nConnections = m_vec.GetSize();
			
			for (nConnectionIndex = 0; nConnectionIndex < nConnections; nConnectionIndex++)
			{
				pT->Lock();
				CComPtr<IUnknown> sp = m_vec.GetAt(nConnectionIndex);
				pT->Unlock();
				IDispatch* pDispatch = reinterpret_cast<IDispatch*>(sp.p);
				if (pDispatch != NULL)
				{
					pvars[0] = nID;
					DISPPARAMS disp = { pvars, NULL, 1, 0 };
					pDispatch->Invoke(0x1, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &disp, NULL, NULL, NULL);
				}
			}
			delete[] pvars;
		}
		catch (...)
		{
			// In debug mode warn the user
			_ASSERTE( FALSE );
		}
	}

	HRESULT Fire_Change(LONG nID, LONG nNewPage)
	{
		try
		{
			CComVariant varResult;
			T* pT = static_cast<T*>(this);
			int nConnectionIndex;
			CComVariant* pvars = new CComVariant[2];
			int nConnections = m_vec.GetSize();
			
			for (nConnectionIndex = 0; nConnectionIndex < nConnections; nConnectionIndex++)
			{
				pT->Lock();
				CComPtr<IUnknown> sp = m_vec.GetAt(nConnectionIndex);
				pT->Unlock();
				IDispatch* pDispatch = reinterpret_cast<IDispatch*>(sp.p);
				if (pDispatch != NULL)
				{
					VariantClear(&varResult);
					pvars[1] = nID;
					pvars[0] = nNewPage;
					DISPPARAMS disp = { pvars, NULL, 2, 0 };
					pDispatch->Invoke(0x6, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &disp, &varResult, NULL, NULL);
				}
			}
			delete[] pvars;
			return varResult.scode;
		}
		catch (...)
		{
			// In debug mode warn the user
			_ASSERTE( FALSE );

			// Return failure
			return E_FAIL;
		}
	}
};


template <class T>
class CProxyICommandEvents : public IConnectionPointImpl<T, &DIID_ICommandEvents, CComDynamicUnkArray>
{
	//Warning this class may be recreated by the wizard.
public:
	VOID Fire_Destroy(LONG nID)
	{
		try
		{
			T* pT = static_cast<T*>(this);
			int nConnectionIndex;
			CComVariant* pvars = new CComVariant[1];
			int nConnections = m_vec.GetSize();
			
			for (nConnectionIndex = 0; nConnectionIndex < nConnections; nConnectionIndex++)
			{
				pT->Lock();
				CComPtr<IUnknown> sp = m_vec.GetAt(nConnectionIndex);
				pT->Unlock();
				IDispatch* pDispatch = reinterpret_cast<IDispatch*>(sp.p);
				if (pDispatch != NULL)
				{
					pvars[0] = nID;
					DISPPARAMS disp = { pvars, NULL, 1, 0 };
					pDispatch->Invoke(0x1, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &disp, NULL, NULL, NULL);
				}
			}
			delete[] pvars;
		}
		catch (...)
		{
			// In debug mode warn the user
			_ASSERTE( FALSE );
		}
	}

	VOID Fire_Hit(LONG nID)
	{
		try
		{
			T* pT = static_cast<T*>(this);
			int nConnectionIndex;
			CComVariant* pvars = new CComVariant[1];
			int nConnections = m_vec.GetSize();
			
			for (nConnectionIndex = 0; nConnectionIndex < nConnections; nConnectionIndex++)
			{
				pT->Lock();
				CComPtr<IUnknown> sp = m_vec.GetAt(nConnectionIndex);
				pT->Unlock();
				IDispatch* pDispatch = reinterpret_cast<IDispatch*>(sp.p);
				if (pDispatch != NULL)
				{
					pvars[0] = nID;
					DISPPARAMS disp = { pvars, NULL, 1, 0 };
					pDispatch->Invoke(0x2, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &disp, NULL, NULL, NULL);
				}
			}
			delete[] pvars;
		}
		catch (...)
		{
			// In debug mode warn the user
			_ASSERTE( FALSE );
		}
	}

	VOID Fire_Unhit(LONG nID)
	{
		try
		{
			T* pT = static_cast<T*>(this);
			int nConnectionIndex;
			CComVariant* pvars = new CComVariant[1];
			int nConnections = m_vec.GetSize();
			
			for (nConnectionIndex = 0; nConnectionIndex < nConnections; nConnectionIndex++)
			{
				pT->Lock();
				CComPtr<IUnknown> sp = m_vec.GetAt(nConnectionIndex);
				pT->Unlock();
				IDispatch* pDispatch = reinterpret_cast<IDispatch*>(sp.p);
				if (pDispatch != NULL)
				{
					pvars[0] = nID;
					DISPPARAMS disp = { pvars, NULL, 1, 0 };
					pDispatch->Invoke(0x3, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &disp, NULL, NULL, NULL);
				}
			}
			delete[] pvars;
		}
		catch (...)
		{
			// In debug mode warn the user
			_ASSERTE( FALSE );
		}
	}

	VOID Fire_Accepted(LONG nID)
	{
		try
		{
			T* pT = static_cast<T*>(this);
			int nConnectionIndex;
			CComVariant* pvars = new CComVariant[1];
			int nConnections = m_vec.GetSize();
			
			for (nConnectionIndex = 0; nConnectionIndex < nConnections; nConnectionIndex++)
			{
				pT->Lock();
				CComPtr<IUnknown> sp = m_vec.GetAt(nConnectionIndex);
				pT->Unlock();
				IDispatch* pDispatch = reinterpret_cast<IDispatch*>(sp.p);
				if (pDispatch != NULL)
				{
					pvars[0] = nID;
					DISPPARAMS disp = { pvars, NULL, 1, 0 };
					pDispatch->Invoke(0x4, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &disp, NULL, NULL, NULL);
				}
			}
			delete[] pvars;
		}
		catch (...)
		{
			// In debug mode warn the user
			_ASSERTE( FALSE );
		}
	}

	VOID Fire_Canceled(LONG nID)
	{
		try
		{
			T* pT = static_cast<T*>(this);
			int nConnectionIndex;
			CComVariant* pvars = new CComVariant[1];
			int nConnections = m_vec.GetSize();
			
			for (nConnectionIndex = 0; nConnectionIndex < nConnections; nConnectionIndex++)
			{
				pT->Lock();
				CComPtr<IUnknown> sp = m_vec.GetAt(nConnectionIndex);
				pT->Unlock();
				IDispatch* pDispatch = reinterpret_cast<IDispatch*>(sp.p);
				if (pDispatch != NULL)
				{
					pvars[0] = nID;
					DISPPARAMS disp = { pvars, NULL, 1, 0 };
					pDispatch->Invoke(0x5, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &disp, NULL, NULL, NULL);
				}
			}
			delete[] pvars;
		}
		catch (...)
		{
			// In debug mode warn the user
			_ASSERTE( FALSE );
		}
	}
};

template <class T>
class CProxyIScrollerEvents : public IConnectionPointImpl<T, &DIID_IScrollerEvents, CComDynamicUnkArray>
{
	//Warning this class may be recreated by the wizard.
public:
	VOID Fire_Destroy(LONG nID)
	{
		try
		{
			T* pT = static_cast<T*>(this);
			int nConnectionIndex;
			CComVariant* pvars = new CComVariant[1];
			int nConnections = m_vec.GetSize();
			
			for (nConnectionIndex = 0; nConnectionIndex < nConnections; nConnectionIndex++)
			{
				pT->Lock();
				CComPtr<IUnknown> sp = m_vec.GetAt(nConnectionIndex);
				pT->Unlock();
				IDispatch* pDispatch = reinterpret_cast<IDispatch*>(sp.p);
				if (pDispatch != NULL)
				{
					pvars[0] = nID;
					DISPPARAMS disp = { pvars, NULL, 1, 0 };
					pDispatch->Invoke(0x1, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &disp, NULL, NULL, NULL);
				}
			}
			delete[] pvars;
		}
		catch (...)
		{
			// In debug mode warn the user
			_ASSERTE( FALSE );
		}
	}

	HRESULT Fire_Change(LONG nID, LONG nX, LONG nY)
	{
		try
		{
			CComVariant varResult;
			T* pT = static_cast<T*>(this);
			int nConnectionIndex;
			CComVariant* pvars = new CComVariant[3];
			int nConnections = m_vec.GetSize();
			
			for (nConnectionIndex = 0; nConnectionIndex < nConnections; nConnectionIndex++)
			{
				pT->Lock();
				CComPtr<IUnknown> sp = m_vec.GetAt(nConnectionIndex);
				pT->Unlock();
				IDispatch* pDispatch = reinterpret_cast<IDispatch*>(sp.p);
				if (pDispatch != NULL)
				{
					VariantClear(&varResult);
					pvars[2] = nID;
					pvars[1] = nX;
					pvars[0] = nY;
					DISPPARAMS disp = { pvars, NULL, 3, 0 };
					pDispatch->Invoke(0x6, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &disp, &varResult, NULL, NULL);
				}
			}
			delete[] pvars;
			return varResult.scode;
		}
		catch (...)
		{
			// In debug mode warn the user
			_ASSERTE( FALSE );

			// Return failure
			return E_FAIL;
		}
	}
};

template <class T>
class CProxyIControlEvents : public IConnectionPointImpl<T, &DIID_IControlEvents, CComDynamicUnkArray>
{
	//Warning this class may be recreated by the wizard.
public:
	VOID Fire_Destroy(LONG nID)
	{
		try
		{
			T* pT = static_cast<T*>(this);
			int nConnectionIndex;
			CComVariant* pvars = new CComVariant[1];
			int nConnections = m_vec.GetSize();
			
			for (nConnectionIndex = 0; nConnectionIndex < nConnections; nConnectionIndex++)
			{
				pT->Lock();
				CComPtr<IUnknown> sp = m_vec.GetAt(nConnectionIndex);
				pT->Unlock();
				IDispatch* pDispatch = reinterpret_cast<IDispatch*>(sp.p);
				if (pDispatch != NULL)
				{
					pvars[0] = nID;
					DISPPARAMS disp = { pvars, NULL, 1, 0 };
					pDispatch->Invoke(0x1, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &disp, NULL, NULL, NULL);
				}
			}
			delete[] pvars;
		}
		catch (...)
		{
			// In debug mode warn the user
			_ASSERTE( FALSE );
		}
	}
};

template <class T>
class CProxyIEditEvents : public IConnectionPointImpl<T, &DIID_IEditEvents, CComDynamicUnkArray>
{
	//Warning this class may be recreated by the wizard.
public:
	VOID Fire_Destroy(LONG nID)
	{
		try
		{
			T* pT = static_cast<T*>(this);
			int nConnectionIndex;
			CComVariant* pvars = new CComVariant[1];
			int nConnections = m_vec.GetSize();
			
			for (nConnectionIndex = 0; nConnectionIndex < nConnections; nConnectionIndex++)
			{
				pT->Lock();
				CComPtr<IUnknown> sp = m_vec.GetAt(nConnectionIndex);
				pT->Unlock();
				IDispatch* pDispatch = reinterpret_cast<IDispatch*>(sp.p);
				if (pDispatch != NULL)
				{
					pvars[0] = nID;
					DISPPARAMS disp = { pvars, NULL, 1, 0 };
					pDispatch->Invoke(0x1, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &disp, NULL, NULL, NULL);
				}
			}
			delete[] pvars;
		}
		catch (...)
		{
			// In debug mode warn the user
			_ASSERTE( FALSE );
		}
	}

	VOID Fire_Begin(LONG nID)
	{
		try
		{
			T* pT = static_cast<T*>(this);
			int nConnectionIndex;
			CComVariant* pvars = new CComVariant[1];
			int nConnections = m_vec.GetSize();
			
			for (nConnectionIndex = 0; nConnectionIndex < nConnections; nConnectionIndex++)
			{
				pT->Lock();
				CComPtr<IUnknown> sp = m_vec.GetAt(nConnectionIndex);
				pT->Unlock();
				IDispatch* pDispatch = reinterpret_cast<IDispatch*>(sp.p);
				if (pDispatch != NULL)
				{
					pvars[0] = nID;
					DISPPARAMS disp = { pvars, NULL, 1, 0 };
					pDispatch->Invoke(0xa, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &disp, NULL, NULL, NULL);
				}
			}
			delete[] pvars;
		}
		catch (...)
		{
			// In debug mode warn the user
			_ASSERTE( FALSE );
		}
	}

	VOID Fire_Change(LONG nID, BSTR strText)
	{
		try
		{
			T* pT = static_cast<T*>(this);
			int nConnectionIndex;
			CComVariant* pvars = new CComVariant[2];
			int nConnections = m_vec.GetSize();
			
			for (nConnectionIndex = 0; nConnectionIndex < nConnections; nConnectionIndex++)
			{
				pT->Lock();
				CComPtr<IUnknown> sp = m_vec.GetAt(nConnectionIndex);
				pT->Unlock();
				IDispatch* pDispatch = reinterpret_cast<IDispatch*>(sp.p);
				if (pDispatch != NULL)
				{
					pvars[1] = nID;
					pvars[0] = strText;
					DISPPARAMS disp = { pvars, NULL, 2, 0 };
					pDispatch->Invoke(0x6, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &disp, NULL, NULL, NULL);
				}
			}
			delete[] pvars;
		}
		catch (...)
		{
			// In debug mode warn the user
			_ASSERTE( FALSE );
		}
	}

	VOID Fire_End(LONG nID, VARIANT_BOOL bSuccess)
	{
		try
		{
			T* pT = static_cast<T*>(this);
			int nConnectionIndex;
			CComVariant* pvars = new CComVariant[2];
			int nConnections = m_vec.GetSize();
			
			for (nConnectionIndex = 0; nConnectionIndex < nConnections; nConnectionIndex++)
			{
				pT->Lock();
				CComPtr<IUnknown> sp = m_vec.GetAt(nConnectionIndex);
				pT->Unlock();
				IDispatch* pDispatch = reinterpret_cast<IDispatch*>(sp.p);
				if (pDispatch != NULL)
				{
					pvars[1] = nID;
					pvars[0] = bSuccess;
					DISPPARAMS disp = { pvars, NULL, 2, 0 };
					pDispatch->Invoke(0xb, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &disp, NULL, NULL, NULL);
				}
			}
			delete[] pvars;
		}
		catch (...)
		{
			// In debug mode warn the user
			_ASSERTE( FALSE );
		}
	}
};
#endif