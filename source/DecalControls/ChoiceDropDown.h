// ChoiceDropDown.h : Declaration of the cChoiceDropDown

#ifndef __CHOICEDROPDOWN_H_
#define __CHOICEDROPDOWN_H_

#include "resource.h"       // main symbols

#include <SinkImpl.h>

class cChoice;

/////////////////////////////////////////////////////////////////////////////
// cChoiceDropDown
class ATL_NO_VTABLE cChoiceDropDown : 
	public CComObjectRootEx<CComMultiThreadModel>,
   public ILayerImpl< cChoiceDropDown >,
   public ILayerRenderImpl,
   public ILayerPopup,
   public cNoEventsImpl,
	public IChoiceDropDown
{
public:
	cChoiceDropDown()
      : m_pChoice( NULL )
	{
	}

   cChoice *m_pChoice;

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cChoiceDropDown)
   COM_INTERFACE_ENTRY(ILayerRender)
   COM_INTERFACE_ENTRY(ILayerPopup)
   COM_INTERFACE_ENTRY(ILayer)
	COM_INTERFACE_ENTRY(IChoiceDropDown)
END_COM_MAP()

// IChoiceDropDown
public:

   // ILayerRender Methods
   STDMETHOD(Reformat)();

   // ILayerPopup Methods
   STDMETHOD(PopupCancel)(MouseState *pMS, VARIANT_BOOL *pbContinue);
};

#endif //__CHOICEDROPDOWN_H_
